/**
 * Project Name:fundamental
 * File Name:MyShopping.java
 * Package Name:com.jiuxing.fundamental.chapter11
 * Date:2017年10月20日上午12:12:19
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MyShopping.java
 * Package Name:com.jiuxing.fundamental.chapter11
 * Date:2017年10月20日上午12:12:19
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter11;

import java.util.Scanner;

/**
 * ClassName:MyShopping <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月20日 上午12:12:19 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MyShopping <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月20日 上午12:12:19 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class MyShopping {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		Menu menu = new Menu();
		menu.showWelcomeMenu();

	}

}

class Menu {
	
	Scanner scanner = new Scanner(System.in);
	/**
	 * 
	 * showWelcomeMenu:显示欢迎菜单. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void showWelcomeMenu(){
		boolean retry = false;
		
		do {
			retry = false;//默认不重试
			/*
			 * 打印菜单选项信息
			 */
			System.out.println("\n\t欢迎使用我行我素购物管理系统");
			System.out.println("\t\t1.登录系统");
			System.out.println("\t\t2.退出");
			System.out.println("****************************************");
			System.out.print("请选择,输入数字:");
			
			int select = scanner.nextInt();
			switch(select){
			case 1:{
					StartSMS startSMS = new StartSMS();
					startSMS.scanner = scanner;
					boolean checked = startSMS.signIn();
					if(checked){
						showMainMenu(); 
					} else {
						retry = true;
					}
				}
				break;
			case 2:
				logout();
				break;
			default:
				System.out.println("请输入正确的选项!");
				retry = true;
			}
		} while(retry);
		
	}
	
	/**
	 * 
	 * showMainMenu:显示主菜单. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void showMainMenu(){
		/*
		 * 打印菜单选项信息
		 */
		System.out.println("\t我行我素购物管理系统主菜单");
		System.out.println("****************************************");
		System.out.println("\t\t1.客户信息管理");
		System.out.println("\t\t2.真情回馈");
		System.out.println("****************************************");
		System.out.print("请选择,输入数字或按0返回上一级菜单:");
		
		int select = scanner.nextInt();
		switch(select){
		case 1:
			showCrmMenu();
			break;
		case 2:
			showFeedback();
			break;
		case 0:
			showWelcomeMenu();
			break;
		}
	}
	
	public void showCrmMenu(){
		
	}
	
	/**
	 * 
	 * showFeedback:显示真情回馈菜单. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void showFeedback(){
		/*
		 * 打印菜单选项信息
		 */
		System.out.println("\t我行我素购物管理系统> 真情回馈");
		System.out.println("****************************************");
		System.out.println("\t\t1.幸运大放送");
		System.out.println("\t\t2.幸运抽奖");
		System.out.print("请选择,输入数字或按0返回上一级菜单:");
		
		int select = scanner.nextInt();
		switch(select){
		case 1:
			System.out.println("幸运大放送");
			break;
		case 2:
			System.out.println("幸运抽奖");
			break;
		case 0:
			this.showMainMenu();
			break;
		}
	}
	
	/**
	 * 
	 * logout:退出. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void logout(){
		System.out.println("已退出系统！");
		System.exit(0);
	}
}

