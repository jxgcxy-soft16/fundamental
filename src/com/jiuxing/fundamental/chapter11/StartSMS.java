/**
 * Project Name:fundamental
 * File Name:StartSMS.java
 * Package Name:com.jiuxing.fundamental.chapter11
 * Date:2017年10月20日上午1:26:20
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:StartSMS.java
 * Package Name:com.jiuxing.fundamental.chapter11
 * Date:2017年10月20日上午1:26:20
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter11;

import java.util.Scanner;

/**
 * ClassName:StartSMS <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月20日 上午1:26:20 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: StartSMS <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月20日 上午1:26:20 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class StartSMS {

	Scanner scanner = null;
	/**
	 * 
	 * signIn:登录验证. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @return true - 登录成功; false - 登录失败
	 * @since JDK 1.6
	 */
	public boolean signIn(){
		boolean ret = false;
		if(scanner==null){
			System.out.print("scanner未创建 ...");
			return ret;
		}
		String rname = "JadeBird",rpwd="0000";
		String name = "";
		String pwd = "";
		System.out.print("请输入用户名:");
		name = scanner.next();
		System.out.print("请输入密码:");
		pwd = scanner.next();
		if(rname.equals(name)&&rpwd.equals(pwd)){
			System.out.println("@@登录成功,"+name+"@@");
			ret = true;
		} else {
			System.out.println("@@你没有权限进入系统,请重新登录。@@");
			ret = false;
		}
		
		return ret;
	}
}

