/**
 * Project Name:fundamental
 * File Name:CustomerBizTest.java
 * Package Name:com.jiuxing.fundamental.chapter13
 * Date:2017年10月26日下午3:33:44
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:CustomerBizTest.java
 * Package Name:com.jiuxing.fundamental.chapter13
 * Date:2017年10月26日下午3:33:44
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter13;

import java.util.Scanner;

/**
 * ClassName:CustomerBizTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月26日 下午3:33:44 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: CustomerBizTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月26日 下午3:33:44 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class CustomerBizTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		CustomerBiz customerBiz = new CustomerBiz();
		String flag = "";
		do {
			System.out.print("请输入客户的姓名:");
			String name = scanner.next();
			customerBiz.addName(name);
			System.out.print("继续输入吗?(y/n):");
			flag = scanner.next();
		} while("y".equals(flag));
		
		customerBiz.showNames();
		
		System.out.print("请输入要查找的客户姓名:");
		String name = scanner.next();
		boolean found = customerBiz.search(name);
		System.out.println("*****查找结果*****");
		if(found){
			System.out.println("找到了:)"); 
		} else {
			System.out.println("没找到:(");
		}
		
		
	}

}

