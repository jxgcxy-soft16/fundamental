/**
 * Project Name:fundamental
 * File Name:CustomerBiz.java
 * Package Name:com.jiuxing.fundamental.chapter13
 * Date:2017年10月26日下午3:13:24
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:CustomerBiz.java
 * Package Name:com.jiuxing.fundamental.chapter13
 * Date:2017年10月26日下午3:13:24
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter13;
/**
 * ClassName:CustomerBiz <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月26日 下午3:13:24 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: CustomerBiz <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月26日 下午3:13:24 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class CustomerBiz {

	String[] names = new String[3];
	/**
	 * 
	 * addName:添加客户名字. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param name
	 * @since JDK 1.6
	 */
	public void addName(String name){
		/* 判断参数是否为空或空串*/
		if(null==name||"".equals(name)){
			System.out.println("参数为空 !");
			return;
		}
		
		/* 查找空置的元素索引位置 */
		int i = 0;
		for(;i<names.length&&names[i]!=null;i++);
		
		/**
		 * 根据索引位置的值判断是否可以添加名字
		 */
		if(i==names.length){
			System.out.println("数组空间已用完，不能再添加!");
		} else {
			names[i] = name;
		}
	}
	
	/**
	 * 
	 * showNames:显示客户名字. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void showNames(){
		System.out.println("********************************");
		System.out.println("           客户姓名列表                  ");  
		System.out.println("********************************");
		for(int i=0;i<names.length&&names[i]!=null;i++){
			System.out.print(names[i]+" ");
		}
	}
	
	/**
	 * 
	 * search:查找客户名. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param name
	 * @return
	 * @since JDK 1.6
	 */
	public boolean search(String name){
		for(int i=0;i<names.length&&names[i]!=null;i++){
			if(names[i].equals(name)){
				return true;
			}
		}
		return false;
	}
}

