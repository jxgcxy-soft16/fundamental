/**
 * Project Name:fundamental
 * File Name:StrTest1.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年11月27日下午2:22:31
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:StrTest1.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年11月27日下午2:22:31
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter06;
/**
 * ClassName:StrTest1 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月27日 下午2:22:31 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: StrTest1 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月27日 下午2:22:31 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class StrTest1 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		String s1 = "hello",s2 = "world";
		String s3 = "hello";
		System.out.println("s1==s3: "+(s1==s3));
		
		String s = "我是程序员，我在学java";
		String sr = s.replace('我', '我');
		System.out.println("s==sr: "+(s==sr));
		System.out.println(sr);
		
		int j = 1234567;
		int k = j,counter = 0;
		while(k!=0){
			counter++;
			k/=10;
		}
		String sNumber = String.valueOf(j);
		System.out.println("j 是"+sNumber.length()+"位数。");
		System.out.println("i 是"+counter+"位数。");
		String ss = "Mary,F,1976";
		String[] slice = ss.split(",");
		for(int i=0;i<slice.length;i++){
			System.out.println(slice[i]);
		}
		
		
	}

}

