/**
 * Project Name:fundamental
 * File Name:MathTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年12月4日上午8:59:13
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter06;

import java.io.File;
import java.io.IOException;

/**
 * ClassName:MathTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月4日 上午8:59:13 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class MathTest {

	public static void main(String[] args) throws IOException {
		double a = Math.random();
		double b = Math.random();
		System.out.println("a*a+b*b="+Math.sqrt(a*a+b*b));
		File f = new File("D:\\download\\education\\课件\\JAVA课件\\java\\TestAccess.java");
		System.out.println("f's getName()="+f.getName());
		System.out.println("f's getPath()="+f.getPath());
		File f2 = new File("D:\\download\\education\\课件\\JAVA课件\\java\\abc.txt");
		boolean cf = f2.createNewFile();
		System.out.println("cf="+cf);
		boolean df = f2.delete();
		System.out.println("delete file 's result df="+df);
	}

}

