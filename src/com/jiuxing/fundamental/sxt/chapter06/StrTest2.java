/**
 * Project Name:fundamental
 * File Name:StrTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年11月27日下午5:03:09
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter06;
/**
 * ClassName:StrTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月27日 下午5:03:09 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class StrTest2 {

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		String str = "AdsfdsfSdfkkUkoiJiikk329kk_=)";
		int lcnt=0,ucnt=0,ocnt=0;
		for(int i=0;i<str.length();i++){
			char ch = str.charAt(i);
			if(ch>='a'&&ch<='z'){
				lcnt++;
			} else if(ch>='A'&&ch<='Z'){
				ucnt++;
			} else {
				ocnt++;
			}
		}
		System.out.println("小写字母个数:"+lcnt);
		System.out.println("大写字母个数:"+ucnt);
		System.out.println("其它字符个数:"+ocnt);
		
		String s2 = "The fox jump fox the grass, the  cat, dog fox abababa";
		System.out.println("fox occurs num:"+strOccurNum(s2, "aba"));

	}
	
	static int strOccurNum(String str, String substr){
		if(null==str ||substr==null||"".equals(str.trim())||"".equals(substr.trim())){
			throw new java.lang.IllegalArgumentException("参数错误");
		}
		int counter = 0;
		int beginIndex = 0;
		while((beginIndex=str.indexOf(substr,beginIndex))!=-1){
			counter++;
			beginIndex += substr.length();
		}
		
		return counter;
	}

}

