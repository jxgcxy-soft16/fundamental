/**
 * Project Name:fundamental
 * File Name:DayTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年12月4日上午11:27:26
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter06;
/**
 * ClassName:DayTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月4日 上午11:27:26 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class DayTest2 {
	enum Sex {
		MAN,WOMAN;
	}
	public static void main(String[] args){
		Day day = Day.valueOf(args[0]);
		switch(day){
		case SUN:
			System.out.println("今天是星期日!");
			break;
		case MON:
			System.out.println("今天是星期一!");
			break;
		case TUE:
			System.out.println("今天是星期二!");
			break;
		case WED:
			System.out.println("今天是星期三!");
			break;
		case THU:
			System.out.println("今天是星期四!");
			break;
		case FRI:
			System.out.println("今天是星期五!");
			break;
		case SAT:
			System.out.println("今天是星期六!");
			break;
		default:
			System.out.println("今天是星期八!");
		
		}
	}
}
