/**
 * Project Name:fundamental
 * File Name:FileList.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年12月4日下午9:46:57
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter06;

import java.io.File;
import java.io.IOException;

/**
 * ClassName:FileList <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月4日 下午9:46:57 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class FileList {

	public static void main(String[] args) throws IOException {

		// TODO Auto-generated method stub
		String fn = "D:\\download\\two";
		File f = new File(fn);
		walkin(f);
//		System.out.println(printDirectoryTree(f));
//		System.out.println("");
		
	}
	
	public static String printDirectoryTree(File folder) {
	    if (!folder.isDirectory()) {
	        throw new IllegalArgumentException("folder is not a Directory");
	    }
	    int indent = 0;
	    StringBuilder sb = new StringBuilder();
	    printDirectoryTree(folder, indent, sb);
	    return sb.toString();
	}

	private static void printDirectoryTree(File folder, int indent,
	        StringBuilder sb) {
	    if (!folder.isDirectory()) {
	        throw new IllegalArgumentException("folder is not a Directory");
	    }
	    sb.append(getIndentString(indent));
	    sb.append("+--");
	    sb.append(folder.getName());
	    sb.append("/");
	    sb.append("\n");
	    for (File file : folder.listFiles()) {
	        if (file.isDirectory()) {
	            printDirectoryTree(file, indent + 1, sb);
	        } else {
	            printFile(file, indent + 1, sb);
	        }
	    }

	}
	
	private static void printFile(File file, int indent, StringBuilder sb) {
	    sb.append(getIndentString(indent));
	    sb.append("+--");
	    sb.append(file.getName());
	    sb.append("\n");
	}

	private static String getIndentString(int indent) {
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < indent; i++) {
	        sb.append("|  ");
	    }
	    return sb.toString();
	}
	
	public static void walkin(File dir) {

	    File listFile[] = dir.listFiles();
	    if (listFile != null) {
	        for (int i=0; i<listFile.length; i++) {
	            if (listFile[i].isDirectory()) {
	              System.out.println("|\t\t");  
	              walkin(listFile[i]);
	            } else {

	                System.out.println("+---"+listFile[i].getName().toString());

	            }
	        }
	    }
	}

}

