/**
 * Project Name:fundamental
 * File Name:FileTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年12月4日下午4:06:30
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter06;

import java.io.File;
import java.io.IOException;

/**
 * ClassName:FileTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月4日 下午4:06:30 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class FileTest {

	public static void main(String[] args) {

		String separator = File.separator;
		String fileName = "myfile.txt";
		String directory = "mydir1"+separator+"mydir2"
		+separator+fileName;
		File f = new File(directory);
		System.out.println("user.dir="+System.getProperty("user.dir"));
		if(f.exists()){
			System.out.println("文件名:"+f.getAbsolutePath());
			System.out.println("文件大小:"+f.length());
		} else {
			f.getParentFile().mkdirs();
			try {
				f.createNewFile();
			} catch (IOException ioe){
				ioe.printStackTrace();
			}
		}
		// TODO Auto-generated method stub

	}

}

