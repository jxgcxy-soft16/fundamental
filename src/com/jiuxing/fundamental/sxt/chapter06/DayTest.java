/**
 * Project Name:fundamental
 * File Name:Day.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年12月4日上午11:19:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Day.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年12月4日上午11:19:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter06;
/**
 * ClassName:Day <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月4日 上午11:19:28 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Day <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月4日 上午11:19:28 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
 enum Day {
	SUN,MON,TUE,WED,THU,FRI,SAT;
}
  public class DayTest {
	enum Sex {
		MAN("男"),WOMAN("女");
		Sex(String value){
			this.value = value;
		}
		
		public final String value(){
			return this.value;
		}
		
		private final String value;
		
		public String toString(){
			
			return new StringBuilder(this.name())
					.append("=").append(this.value).toString();
		}
	}
	public static void main(String[] args){
		System.out.println(Sex.MAN+","+Sex.WOMAN);
		/*Day day = Day.valueOf(args[0]);
		switch(day){
		case SUN:
			System.out.println("今天是星期日!");
			break;
		case MON:
			System.out.println("今天是星期一!");
			break;
		case TUE:
			System.out.println("今天是星期二!");
			break;
		case WED:
			System.out.println("今天是星期三!");
			break;
		case THU:
			System.out.println("今天是星期四!");
			break;
		case FRI:
			System.out.println("今天是星期五!");
			break;
		case SAT:
			System.out.println("今天是星期六!");
			break;
		default:
			System.out.println("今天是星期八!");
		
		}*/
	}
}



