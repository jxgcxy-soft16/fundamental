/**
 * Project Name:fundamental
 * File Name:StrTest3.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年11月29日上午9:27:05
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:StrTest3.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter06
 * Date:2017年11月29日上午9:27:05
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter06;
/**
 * ClassName:StrTest3 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月29日 上午9:27:05 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: StrTest3 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月29日 上午9:27:05 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class StrTest3 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		String param = "1,2;3,4,5;6,7,8";
		String[] digitals = param.split(",|;");
		System.out.println(digitals);
		for(String d:digitals){
		  System.out.print(d+" ");	
		}
		/*double[][] dArr = strToDoubleArray(param);
		for(int i=0;i<dArr.length&&null!=dArr[i];i++){
			for(int j=0;j<dArr[i].length;j++){
				StringBuilder sb = new StringBuilder("d[").append(i)
				.append(",").append(j).append("]=")
				.append(dArr[i][j]).append(" ");
				System.out.print(sb);
			}
			System.out.println();
		}*/
	}
	
	public static double[][] strToDoubleArray(String str){
		String[] stArr = str.split(";");
		if(stArr.length>0){
			double[][] dbArr = new double[stArr.length][];
			for(int i=0;i<stArr.length;i++){
				if(null!=stArr[i]&&!"".equals(stArr[i].trim())){
					String[] subStArr = stArr[i].split(",");
					dbArr[i] = new double[subStArr.length];
					for(int j=0;j<subStArr.length;j++){
						dbArr[i][j] = Double.parseDouble(subStArr[j]);
					}
				}
			}
			return dbArr;
		}
		return null;
	}

}

