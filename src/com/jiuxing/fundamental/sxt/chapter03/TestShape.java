/**
 * Project Name:fundamental
 * File Name:TestShape.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月20日上午10:31:17
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TestShape.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月20日上午10:31:17
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:TestShape <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月20日 上午10:31:17 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TestShape <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月20日 上午10:31:17 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TestShape {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Shape rect = new Rectangle(40,30);
		Shape tri = new Triangle(20,20);
		Shape circle = new Circle2(30);
		
		System.out.println("rect's area:"+rect.calcArea());
		System.out.println("tri's area:"+tri.calcArea());
		System.out.println("circle's area:"+circle.calcArea());
	}

}

abstract class Shape {
	Shape(){}
	public abstract double calcArea();
}

class Rectangle extends Shape {

	private double width;
	private double height;
	
	
	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public double calcArea() {
		// TODO Auto-generated method stub
		double area = this.width*this.height;
		return area;
	}
}

class Triangle extends Shape {

	private double width;
	private double height;
	
	
	public Triangle(double width, double height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public double calcArea() {
		
		// TODO Auto-generated method stub
		double area = width*height/2;
		return area;
	}
}

class Circle2 extends Shape {

	private double radius;
	
	
	public Circle2(double radius) {
		this.radius = radius;
	}


	@Override
	public double calcArea() {
		
		// TODO Auto-generated method stub
		double area = Math.PI*radius*radius;
		return area;
	}
	
}

