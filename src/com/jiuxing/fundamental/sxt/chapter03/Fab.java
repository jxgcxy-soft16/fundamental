/**
 * Project Name:fundamental
 * File Name:Fab.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月24日上午11:23:19
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Fab.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月24日上午11:23:19
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:Fab <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月24日 上午11:23:19 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Fab <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月24日 上午11:23:19 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Fab {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub

		int n = 4;
		int result = fab(n);
		System.out.println("result="+result);
		int result2 = evalByLoop(n);
		System.out.println("result2="+result2);
		
		
	}
	
	/**
	 * 斐波那契数列递归求第n个数的值
	 * fab:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param n
	 * @return
	 * @since JDK 1.6
	 */
	static int fab(int n){
		if(n==1||n==2){
			return 1;
		} else {
			return fab(n-1)+fab(n-2);
		}
	}
	
	/**
	 * 
	 * evalByLoop:斐波那契数列循环求第n个数的值. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @return
	 * @since JDK 1.6
	 */
	static int evalByLoop(int n){
		if(n==1||n==2){
			return 1;
		}
		
		//n>2,通过下面循环来计算
		int f3 = 0;
		int counter = 2;
		for(int f1=1,f2=1;counter<n;
				f1 = f2,f2 = f3){
			f3 = f1+f2;
			counter++;
		}
		return f3;
	}

}

