/**
 * Project Name:fundamental
 * File Name:TestCircle.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月14日下午4:52:12
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:TestCircle <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月14日 下午4:52:12 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class TestCircle {

	public static void main(String[] args) {

		Circle circle = new Circle(-20, 30, 10);
		Point2 p = new Point2(-10,30);
		boolean coned = circle.contains(p);
		System.out.println("coned="+coned);
		

	}

}

class Point2 {
	private double x;
	private double y;
	
	Point2(double x, double y){
		this.x = x;
		this.y = y;
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public double calcDistance(Point2 p){
		double dist = (this.x - p.getX())*(this.x - p.getX())
				+(this.y - p.getY())*(this.y - p.getY());
		dist = Math.sqrt(dist);
		return dist;
	}
}

class Circle {
	private Point2 o;
	private double radius;
	
	Circle(){
		this(0,0, 10);
	}
	
    Circle(Point2 o, double radius){
		this.o = o;
		this.radius = radius;
	}
    
    Circle(double x, double y, double radius){
		this(new Point2(x, y), radius);
	}
	
	public Point2 getO() {
		return o;
	}
	public void setO(Point2 o) {
		this.o = o;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public boolean contains(Point2 p){
		double dist = o.calcDistance(p);
		return (dist<=this.radius);
	}
	
}