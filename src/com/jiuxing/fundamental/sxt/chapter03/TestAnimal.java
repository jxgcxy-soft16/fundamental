/**
 * Project Name:fundamental
 * File Name:TestAnimal.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月17日上午11:01:27
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:TestAnimal <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月17日 上午11:01:27 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class TestAnimal {

	public static void main(String[] args) {
		TestAnimal t = new TestAnimal();
		Animal a = new Animal("动物");
		Cat c = new Cat("tom","ssfwf");
		Dog d = new Dog("汪星人","sdfs");
		t.f(a);
		t.f(c);
		t.f(d);
		
	}
	
	public void f(Animal a){
		System.out.println("\n\nanimal's name:"+a.name);
		if(a instanceof Cat){
			Cat c = (Cat)a;
			System.out.println("This animal is a cat,its eyes color is "+c.eyesColor);
		} else if (a instanceof Dog){
			Dog d = (Dog)a;
			System.out.println("This animal is a dog,its fur color is "+d.furColor);
		} else {
			System.out.println("unknow name ...");
		}
	}

}

class Animal {
	public String name;
	Animal(String name){
		this.name = name;
	}
}

class Cat extends Animal {
	public String eyesColor;
	Cat(String name, String color){
		super(name);
		this.eyesColor = color;
	}
}

class Dog extends Animal {
	public String furColor;
	Dog(String name, String furColor){
		super(name);
		this.furColor = furColor;
	}
}

