/**
 * Project Name:fundamental
 * File Name:Person.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月14日下午3:15:01
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:Person <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月14日 下午3:15:01 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Person {

	int id;
	int age;
	
	
	Person(){
		this(20);
		/*this.id = 0;
		this.age = 20;*/
	}
	
	Person(int a){
		this(0, a);
		/*this.id = 0;
		this.age = a;
		
		this.Person(0,a);*/
	}
	
	Person(int i, int a){
		this.id = i;
		this.age = a;
	}
	
	
	public void setAge(int age) {
		this.age = age;
	}

	public String toString(){
		return "{id:"+this.id
				+", age:"+this.age+"}";
		
	}
	
	public static void main(String[] args){
		Person p1 = new Person(1,22);
		Person p2 = new Person(2,23);
//		Person p3 = new Person(88, 60);
		int age = 25;
		p1.setAge(age);
		p2.setAge(age++);
		
		System.out.println("p1="+p1);
		System.out.println("p2="+p2);
//		System.out.println("p3="+p3);
	}
}

