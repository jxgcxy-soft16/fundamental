/**
 * Project Name:fundamental
 * File Name:Flyable.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月20日上午11:02:55
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Flyable.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月20日上午11:02:55
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:Flyable <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月20日 上午11:02:55 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Flyable <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月20日 上午11:02:55 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public  interface Flyable {

	public static final int id = 0;
	 abstract void fly();
}

interface Jumpable {
	void jump();
}

interface FlyableEx extends Flyable,Jumpable {
	
}

class Something implements Flyable,Jumpable {

	@Override
	public void jump() {
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fly() {
		
		// TODO Auto-generated method stub
		
	}
	
}

