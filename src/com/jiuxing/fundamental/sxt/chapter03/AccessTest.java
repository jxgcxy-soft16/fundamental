/**
 * Project Name:fundamental
 * File Name:AccessTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2018年1月9日上午9:48:40
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:AccessTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2018年1月9日上午9:48:40
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:AccessTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月9日 上午9:48:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: AccessTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月9日 上午9:48:40 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class AccessTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub

	}

}
class Parent2 {
	private int n_private = 1;
			int n_friendly = 2;
	protected int n_protected = 3;
	public  int n_public = 4;
}

class Child2 extends Parent2 {
	public void f(){
//		n_private = 10;
		n_friendly= 20;
		n_protected = 30;
		n_public = 40;
	}
	
}

