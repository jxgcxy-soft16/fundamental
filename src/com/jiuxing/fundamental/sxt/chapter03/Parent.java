/**
 * Project Name:fundamental
 * File Name:Parent.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月14日上午9:53:18
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:Parent <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月14日 上午9:53:18 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Parent {

	private int privateVal = 1;
			int defaultVal = 2;
	protected int protectVal = 3;
	public int publicVal = 4;
	
	public String toString(){
		return "{privateVal:"+privateVal
				+", defaultVal:"+defaultVal
				+", protectVal:"+protectVal
				+", publicVal:" +publicVal
				+"}";
	}
	public static void main(String[] args){
		Parent p = new Parent();
		Child c = new Child();
		c.reassignVal();
		System.out.println("p="+p);
		System.out.println("c="+c);
	}
}

class Child extends Parent {
	public void reassignVal(){
//		privateVal = 100;
		defaultVal = 200;
		protectVal = 300;
		publicVal = 400;
	}
}

