/**
 * Project Name:fundamental
 * File Name:PetCarerTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月20日下午5:07:47
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:PetCarerTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月20日 下午5:07:47 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class PetCarerTest {

	public static void main(String[] args) {
		PetCarer pc1 = new Worker();
		PetCarer pc2 = new Peasant();
		PetCarer pc3 = new Offical();
		
		pc1.feed();
		pc1.play();
		
		pc2.feed();
		pc2.play();
		
		pc3.feed();
		pc3.play();
	}

}

interface PetCarer {
	/**
	 * 
	 * feed:喂食. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	void feed();
	
	/**
	 * 
	 * play:玩. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	void play();
}

class Worker implements PetCarer {

	@Override
	public void feed() {
		
		// TODO Auto-generated method stub
		System.out.println("工人喂食");
		
	}

	@Override
	public void play() {
		
		// TODO Auto-generated method stub
		System.out.println("工人陪玩");
		
	}
	
}

class Peasant implements PetCarer {

	@Override
	public void feed() {
		System.out.println("农民喂食");
	}

	@Override
	public void play() {
		System.out.println("农民陪玩");
	}
	
}

class Offical implements PetCarer {

	@Override
	public void feed() {
		System.out.println("国家干部喂食");
	}

	@Override
	public void play() {
		System.out.println("国家干部陪玩");
	}
	
}

