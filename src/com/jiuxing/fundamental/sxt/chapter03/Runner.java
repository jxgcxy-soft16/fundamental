/**
 * Project Name:fundamental
 * File Name:Runner.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2018年1月9日上午11:29:52
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:Runner <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月9日 上午11:29:52 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public interface Runner {

	 int id = 1;
	void start();
	void stop();
	void run();
}

interface Jumpable2 {
	void jump();
}
@FunctionalInterface
interface Swimmable {
	void swim();
}
interface Flyable2 {
	void fly();
}
interface movable extends Jumpable2,Swimmable,Flyable2 {
	int MOVE_ID = 0;
	void move();
	/*default void move(){
		
	}*/
}

