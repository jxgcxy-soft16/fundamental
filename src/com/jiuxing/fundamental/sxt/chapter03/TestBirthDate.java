/**
 * Project Name:fundamental
 * File Name:BirthDate.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月13日下午3:15:09
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:BirthDate <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月13日 下午3:15:09 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class TestBirthDate {

	
	public static void main(String[] args){
		TestBirthDate test = new TestBirthDate();
		int date = 9;
		BirthDate d1 = new BirthDate(7,7,1970);
		BirthDate d2 = new BirthDate(1,1,2000);
		
		test.change1(date);
		test.change2(d1);
		test.change3(d2);
		
		System.out.println("date="+date);
		d1.display();
		d2.display();
	}
	
	public void change1(int i){
		i = 1234;
	}
	
	public void change2(BirthDate d){
		d = new BirthDate(22,2,2004);
	}
	
	public void change3(BirthDate d){
		d.setDay(22);
	}
}

