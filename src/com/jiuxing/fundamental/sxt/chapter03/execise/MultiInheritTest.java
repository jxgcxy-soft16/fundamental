/**
 * Project Name:fundamental
 * File Name:MultiInheritTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03.execise
 * Date:2017年11月24日上午1:06:16
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03.execise;
/**
 * ClassName:MultiInheritTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月24日 上午1:06:16 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class MultiInheritTest implements InterA,InterB {

	public static void main(String[] args) {

		// TODO Auto-generated method stub

	}

	@Override
	public String ball() {
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int balla() {
		
		// TODO Auto-generated method stub
		return 0;
	}

	

}

interface InterA {
	int balla();
}
interface InterB {
	String ball();
}

