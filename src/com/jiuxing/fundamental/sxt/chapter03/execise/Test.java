/**
 * Project Name:fundamental
 * File Name:Test.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03.execise
 * Date:2017年11月15日下午4:51:56
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03.execise;
/**
 * ClassName:Test <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月15日 下午4:51:56 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Test {

	public static void main(String[] args) {
		
		Person p1 = new Person("A");
		Person p2 = new Person("A2","beijing");
		Student s1 = new Student("C", "S1");
		Student s2 = new Student("C", "shanghai","S2");
//		Teacher t1 = new Teacher("lucy","T1");
//		Teacher t2 = new Teacher("lily","guangzhou","T2");
		
		System.out.println(p1.equals(p2));
		System.out.println("p1="+p1);
		System.out.println("p2="+p2);
		int[] a = new int[0];
		System.out.println("p3="+(a instanceof Object));
		/*System.out.println(p1.info());
		System.out.println(p2.info());
		System.out.println(s1.info());
		System.out.println(s2.info());
		System.out.println(t1.info());
		System.out.println(t2.info());*/
	}

}

class Person {
	/*private*/ String name;
	/*private*/ String location;
	
	Person(String name){
		this.name = name;
		this.location = "beijing";
	}
	
	Person(String name, String location){
		this.name = name;
		this.location = location;
	}
	
	public String info(){
		return "name: "+name+
				" location: "+location;
	}
	
	public boolean equals(Object obj) {
		if(this == obj){
			return true;
		}
		
		if(obj instanceof Person){
			
			Person p = (Person)obj;
			if(null==this.name||null==this.location){
				return false;
			}
			if(this.name.equals(p.name)
				&&this.location.equals(p.location)){
				return true;
			}
		}
        return false;
    }
	
	public  int hashCode(){
		final int prime = 31;
		int result = prime +this.name.hashCode()+this.location.hashCode();
		return result;
	}
	
	public String toString(){
		return "{name:"+this.name+", location:"+this.location+"}";
	}
}

class Student extends Person {
	private String school;
	
	Student(String name,String location, String school){
		super(name, location);
		this.school = school;
	}
	
	Student(String name, String school){
		this(name, "beijing", school);
	}
	
	public String info(){
		return super.info()+" school: "+school;
	}
	
	public boolean equals(Object obj) {
		if(this == obj){
			return true;
		}
		if(obj instanceof Student){
			if(null==this.school){
				return false;
			}
			Student s = (Student)obj;
			if(super.equals(obj)&&this.school.equals(s.school)){
				return true;
			}
		}
		return false;
	}
	
	public int hashCode(){
		int result = super.hashCode()+(this.school==null?0:this.school.hashCode());
		return result;
	}
	
	public String toString(){
		return super.toString()+",school: "+this.school+"}";/*"{name:"+super.name+", location:"+this.location+"}";*/
	}
		
}

class Teacher extends Person {
	private String title;
	
	Teacher(String name,String location, String title){
		super(name, location);
		this.title = title;
	}
	
	Teacher(String name,String title){
		this(name, "beijing", title);
	}
	
	public String info(){
		return super.info()+" title: "+this.title;
	}
}

