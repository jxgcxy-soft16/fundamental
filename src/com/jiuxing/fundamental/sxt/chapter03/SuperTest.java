/**
 * Project Name:fundamental
 * File Name:SuperTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2018年1月9日上午10:30:33
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:SuperTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2018年1月9日上午10:30:33
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:SuperTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月9日 上午10:30:33 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: SuperTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月9日 上午10:30:33 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class SuperTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		Base b = new Derive();
		b.b();
		System.out.println(b.name);
	}

}
class Base {
	 String name = "abc";
	Base(/*String s*/){
		System.out.println("父类Base的无参构造方法");
	}
	public void b(){
		System.out.println("调用父类方法b()");
	}
}

class Derive extends Base {
	 String name = "abc";
	Derive(){
//		super("");
		System.out.println("子类Derive的无参构造方法");
	}
	public void a(){
		System.out.println("调用方法a()");
	}
	
	public void b(){
		System.out.println("调用子类Derive方法b()");
	}
}
