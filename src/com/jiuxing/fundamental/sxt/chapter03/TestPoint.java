/**
 * Project Name:fundamental
 * File Name:TestPoint.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月13日上午11:54:25
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:TestPoint <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月13日 上午11:54:25 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class TestPoint {

	public static void main(String[] args) {

		// TODO Auto-generated method stub

		Point p0 = new Point(0,0,0);//创建原点
		Point p1 = new Point(23,150,-30);//创建任意点
		
		double d = p1.calcDistance(p0);//计算任意点到原点的距离平方
		System.out.println(p1+"到"+p0+"到原点的距离平方为"+d);
	}

}

class Point {
	/**
	 * x轴坐标
	 */
	private double x;
	
	/**
	 * y轴坐标
	 */
	private double y;
	
	/**
	 * z轴坐标
	 */
	private double z;
	
	/**
	 * 
	 * Creates a new instance of Point.
	 *
	 * @param x
	 * @param y
	 * @param z
	 */
	public Point(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	/**
	 * 
	 * calcDistance:计算到另一点距离平方. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param p 另一点
	 * @return
	 * @since JDK 1.6
	 */
	public double calcDistance(Point p){
		double i = (x-p.getX())*(x-p.getX())
				+(y-p.getY())*(y-p.getY())
				+(z-p.getZ())*(z-p.getZ());
//		i = Math.sqrt(i);
		return i;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("坐标(")
				.append(this.x).append(",")
				.append(this.y).append(",")
				.append(this.z).append(")");
		return sb.toString();
	}
	
	
}

