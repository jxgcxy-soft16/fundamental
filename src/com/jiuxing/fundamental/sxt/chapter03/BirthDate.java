/**
 * Project Name:fundamental
 * File Name:BirthDate.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter03
 * Date:2017年11月13日下午3:15:09
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter03;
/**
 * ClassName:BirthDate <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月13日 下午3:15:09 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class BirthDate {

	private int day;
	private int month;
	private int year;
	
	public BirthDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public void display(){
		System.out.println(day+"-"+month+"-"+year);
	}
	
	
	
	
}

