/**
 * Project Name:fundamental
 * File Name:FileCopyTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月21日下午5:07:36
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:FileCopyTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月21日下午5:07:36
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * ClassName:FileCopyTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月21日 下午5:07:36 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: FileCopyTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月21日 下午5:07:36 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class FileCopyTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		FileCopy fc = new FileCopy();
		fc.copy("D:\\download\\education\\课件\\03Java程序设计[已完成]\\帮助文档\\jdk6.ZH_cn.chm", "F:\\download\\jdk6.ZH_cn.chm");

   }
}

class FileCopy {
	
	void copy(String fromFile, String toFile){
		FileInputStream fis = null;
		FileOutputStream fos = null;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		
		try {
			fis = new FileInputStream(fromFile);
			bis = new BufferedInputStream(fis);
			
			fos = new FileOutputStream(toFile);
			bos = new BufferedOutputStream(fos); 
			byte[] buff = new byte[1024];
			int len = -1;
			while((len = bis.read(buff))!=-1){
				bos.write(buff, 0, len);
			}
			
		}
		catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(bis!=null){
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fis!=null){
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(	bos!=null){
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos!=null){
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

