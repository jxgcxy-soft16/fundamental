/**
 * Project Name:fundamental
 * File Name:WriterTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日下午4:28:38
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:WriterTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日下午4:28:38
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * ClassName:WriterTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月11日 下午4:28:38 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: WriterTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月11日 下午4:28:38 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class WriterTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		String filename = "D:\\download\\education\\demo\\out.txt";
		Writer out = null;
		try {
			out = new OutputStreamWriter(new FileOutputStream(filename,true));
			for(int i=0;i<10;i++){
				out.append("lineno"+(i+1)+"\n");
				out.append("\n");
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(null!=out){
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}

