/**
 * Project Name:fundamental
 * File Name:PrintTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月13日下午2:18:03
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:PrintTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月13日下午2:18:03
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * ClassName:PrintTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月13日 下午2:18:03 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: PrintTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月13日 下午2:18:03 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class PrintTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		testPrintWriter();
		testPrintStream();
	}
	
	static void testPrintWriter(){
		PrintWriter out = null;
		try {
			System.out.println("PrintWriter:把下面内容打印到一个文件...");
			out = new PrintWriter("D:\\download\\education\\demo\\PrintWriter.txt");
			for(int i=0;i<20;i++){
				out.println("sdfwegweggwdsfd历史档案咖啡");
			}
			out.printf("digitals:%10d,%5.2f", 100,2.34566);
			System.out.println("PrintWriter:打印完毕...");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			if(out!=null){
				out.close();
			}
		}
	}
	
	static void testPrintStream(){
		PrintStream out = null;
		try {
			System.out.println("PrintStream:把下面内容打印到一个文件...");
			out = new PrintStream("D:\\download\\education\\demo\\PrintStream.txt");
			out.append("jjkkjljlkjlkklll");
			out.append("666666666666");
//			out.print("end");
//			out.flush();
			System.setOut(out);
			System.out.println("PrintStream:打印完毕...");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}

