/**
 * Project Name:fundamental
 * File Name:FosTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日下午3:16:01
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:FosTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日下午3:16:00
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * ClassName:FosTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月11日 下午3:16:01 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: FosTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月11日 下午3:16:01 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class FosTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		File f = new File("D:\\download\\education\\demo\\abc.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(f,true);
			String s = "\nnextline";
			for(int i=0;i<s.length();i++){
				char c = s.charAt(i);
				fos.write(c);
			}
			System.out.println("文件输出流输出完毕！");
		} 
		catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		finally {
			if(null != fos){
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

