/**
 * Project Name:fundamental
 * File Name:FileInputStreamTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日上午10:42:52
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:FileInputStreamTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日上午10:42:52
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * ClassName:FileInputStreamTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月11日 上午10:42:52 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: FileInputStreamTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月11日 上午10:42:52 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class FileInputStreamTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		File f = new File("D:\\download\\education\\demo\\mytest.txt");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			int d = -1;
			byte[] b = new byte[1024];
			while((d = fis.read(b))!=-1){
				System.out.print(new String(b,0,d));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(null!=fis){
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}

