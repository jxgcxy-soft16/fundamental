/**
 * Project Name:fundamental
 * File Name:ReaderTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日下午4:03:17
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ReaderTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月11日下午4:03:17
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * ClassName:ReaderTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月11日 下午4:03:17 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ReaderTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月11日 下午4:03:17 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ReaderTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		String fileName = "D:\\download\\education\\demo\\abc.txt";
		Reader reader = null;
		try {
			reader = new InputStreamReader(new FileInputStream(fileName)/*,"utf8"*/);
			
			int d;
			char[] ca = new char[100];
			while((d=reader.read(ca))!=-1){
				System.out.print(new String(ca));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(null!=reader){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

