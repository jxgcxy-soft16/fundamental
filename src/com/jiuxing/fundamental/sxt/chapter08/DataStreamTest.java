/**
 * Project Name:fundamental
 * File Name:DataStreamTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午10:56:48
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:DataStreamTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午10:56:48
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * ClassName:DataStreamTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月12日 上午10:56:48 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: DataStreamTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月12日 上午10:56:48 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class DataStreamTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		/*ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			dos.writeDouble(2.3);
			dos.writeInt(1212);
			dos.writeBoolean(false);
			dos.writeChar('A');
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		DataInputStream dis = new DataInputStream(bais);
		try {
			
			System.out.println(dis.readDouble());
			System.out.println(dis.readInt());
			System.out.println(dis.readBoolean());
			System.out.println(dis.readChar());
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		writeByStr();
		writeByData();
	}
	
	static void writeByStr(){
		String filename = "D:\\download\\education\\demo\\writeByStr.dat";
		FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		try {
			fos = new FileOutputStream(filename);
			osw = new OutputStreamWriter(fos);
			StringBuilder sb = new StringBuilder();
			sb.append(2.312345678)
			.append(1234567890)
			.append(false)
			.append('A');
			osw.write(sb.toString());
			osw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(osw!=null){
				try {
					osw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos!=null){
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	static void writeByData(){
		String filename = "D:\\download\\education\\demo\\writeByData.dat";
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		try {
			fos = new FileOutputStream(filename);
			dos = new DataOutputStream(fos);
			dos.writeDouble(2.312345678);
			dos.writeInt(1234567890);
			dos.writeBoolean(false);
			dos.writeChar('A');
			dos.flush();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(dos!=null){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos!=null){
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

