/**
 * Project Name:fundamental
 * File Name:LamdaTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月15日上午8:45:46
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:LamdaTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月15日上午8:45:46
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;
/**
 * ClassName:LamdaTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月15日 上午8:45:46 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: LamdaTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月15日 上午8:45:46 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class LamdaTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Operator add = (int a, int b)->a+b;
		Operator sub = (a, b)->a-b;
		Operator mult = (a, b)->a*b;
		Operator div = (int a, int b)->a/b;
		
		Welcome welcome = (String a)->System.out.println("welcome:"+a);
		
		int a=7,b=5;
		String msg = "hello,tom";
		System.out.println(a+"+"+b+"="+add.operate(a, b));
		System.out.println(a+"-"+b+"="+sub.operate(a, b));
		System.out.println(a+"*"+b+"="+mult.operate(a, b));
		System.out.println(a+"/"+b+"="+div.operate(a, b));
		
		welcome.welcome(msg);

	}

}

interface Operator {
	int operate(int a, int b);
}

interface Welcome {
	void welcome(String msg);
}

