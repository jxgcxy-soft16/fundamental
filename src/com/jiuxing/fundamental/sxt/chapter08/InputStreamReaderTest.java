/**
 * Project Name:fundamental
 * File Name:InputStreamReaderTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午9:58:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:InputStreamReaderTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午9:58:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * ClassName:InputStreamReaderTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月12日 上午9:58:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: InputStreamReaderTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月12日 上午9:58:40 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class InputStreamReaderTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		InputStreamReader is = null;
			
		int d;
		try {
			is = new InputStreamReader(System.in,"ISO8859_1");
			while((d = is.read())!=-1){
				System.out.print((char)d);
			}
		} 
		catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}

