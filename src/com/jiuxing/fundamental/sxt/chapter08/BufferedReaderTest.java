/**
 * Project Name:fundamental
 * File Name:BufferedReaderTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午9:00:10
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:BufferedReaderTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午9:00:10
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * ClassName:BufferedReaderTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月12日 上午9:00:10 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: BufferedReaderTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月12日 上午9:00:10 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class BufferedReaderTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		String filename = "D:\\download\\education\\demo\\mytest.txt";
		Reader in = null;
		BufferedReader br = null;
		try {
			in = new FileReader(filename);
			br = new BufferedReader(in);
			String line = br.readLine();
			while(null!=line){
				System.out.println(line);
				line = br.readLine();
			}
		}
		catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(null!=br){
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null!=in){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

