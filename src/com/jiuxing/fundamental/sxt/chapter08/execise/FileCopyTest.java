/**
 * Project Name:fundamental
 * File Name:FileCopyTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08.execise
 * Date:2017年12月22日上午11:05:29
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:FileCopyTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08.execise
 * Date:2017年12月22日上午11:05:29
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08.execise;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * ClassName:FileCopyTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月22日 上午11:05:29 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: FileCopyTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月22日 上午11:05:29 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class FileCopyTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		if(null!=args&&args.length==2){
			String fromFile = args[0];
			String toDir = args[1];
			cp(fromFile, toDir);
		} else {
			System.out.println("命令运行缺少参数或参数格式不对!");
		}
		
	}
	
	public static void cp(String fromFile, String toDir){
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			if(fromFile.equals(toDir)){
				return;
			}
			File srcFile = new File(fromFile);
			if(!srcFile.exists()||srcFile.isDirectory()){
				return;
			}
			FileInputStream fis = new FileInputStream(srcFile);
			bis = new BufferedInputStream(fis);
			
			File dirFile = new File(toDir);
			if(!dirFile.exists()){
				dirFile.mkdirs();
			}
			
			String fileName = fromFile.substring(fromFile.lastIndexOf(File.separator)+1);
			File dstFile = new File(dirFile,fileName);
			FileOutputStream fos = new FileOutputStream(dstFile);
			bos = new BufferedOutputStream(fos);
			
			byte[] buf = new byte[1024];
			int len = -1;
			while((len = bis.read(buf))!=-1){
				bos.write(buf, 0, len);
			}
		}
		catch(FileNotFoundException  fnfe){
			fnfe.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(bis!=null){
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(bos!=null){
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}

