/**
 * Project Name:fundamental
 * File Name:BufferedOutputStreamTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午9:38:37
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:BufferedOutputStreamTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日上午9:38:37
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * ClassName:BufferedOutputStreamTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月12日 上午9:38:37 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: BufferedOutputStreamTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月12日 上午9:38:37 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class BufferedOutputStreamTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		String filename = "D:\\download\\education\\demo\\abc.txt";
		OutputStream os = null;
		BufferedOutputStream bos = null;
		try {
			os = new FileOutputStream(filename);
			bos = new BufferedOutputStream(os);
			String s = "今天双十二";
			byte[] b = s.getBytes("utf8");
			bos.write(b, 0, b.length);
			bos.flush();
		}
		catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
		}
		catch(IOException ie){
			ie.printStackTrace();
		}
		finally {
			if(null!=bos){
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}

