/**
 * Project Name:fundamental
 * File Name:BufferedWriterTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日下午3:20:52
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:BufferedWriterTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter08
 * Date:2017年12月12日下午3:20:52
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter08;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * ClassName:BufferedWriterTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月12日 下午3:20:52 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: BufferedWriterTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月12日 下午3:20:52 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class BufferedWriterTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		String filename = "D:\\download\\education\\demo\\test1212.txt";
		FileWriter out = null;
		BufferedWriter writer = null;
		try {
			out = new FileWriter(filename);
			writer = new BufferedWriter(out);
			for(int i=0;i<10;i++){
				writer.write("第"+(i+1)+"行");
				writer.newLine();
				System.out.println("第"+(i+1)+"行");
			}
			writer.flush();
			System.out.println("写完毕");
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
					
				}
			}
			if(out!=null){
				try {
					out.close();
				} catch (IOException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
			}
		}
	}

}

