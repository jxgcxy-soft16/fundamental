/**
 * Project Name:fundamental
 * File Name:RunnableTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日下午3:56:58
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:RunnableTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日下午3:56:58
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:RunnableTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月19日 下午3:56:58 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: RunnableTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月19日 下午3:56:58 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class RunnableTest2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Runner runner = new Runner();
		Thread t = new Thread(runner);
		t.start();
		
		
		/*try {
			t.join(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		
		Runner runner2 = new Runner();
		Thread t2 = new Thread(runner2);
		t2.start();
		
		for(int i=0;i<20;i++){
			if(i%3==0){
				Thread.yield();
			}
			System.out.println("当前线程:"+Thread.currentThread().getName()+",i="+i);
		}
//		t.interrupt();
		
	}

}

class Runner implements Runnable {

	private volatile int count = 0;
	@Override
	public void run() {
//		 int  count = 0;
		while(true){
			if(count>=20){
				return;
			}
			if(count%3==0){
				Thread.yield();
			}
			System.out.println("thread["+Thread.currentThread().getName()+"]  run ...,count="+count);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("当前线程:"+Thread.currentThread().getName());
				e.printStackTrace();
				break;
			}
			count++;
		}
	}
	
}

