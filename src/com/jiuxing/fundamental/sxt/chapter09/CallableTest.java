/**
 * Project Name:fundamental
 * File Name:CallableTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2018年1月29日下午2:44:38
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * ClassName:CallableTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月29日 下午2:44:38 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class CallableTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		// TODO Auto-generated method stub
		Callable<Integer> call = new RetThread<Integer>();
		FutureTask<Integer> ft = new FutureTask<>(call);
		new Thread(ft,"有返回值的线程").start();
		System.out.println(ft.get());

	}

}

class RetThread<Integer> implements Callable<java.lang.Integer> {

	@Override
	public java.lang.Integer call() throws Exception {
		
		// TODO Auto-generated method stub
		java.lang.Integer v = new java.lang.Integer(100);
		TimeUnit.SECONDS.sleep(1);
		
		return v;
	}
	
}

