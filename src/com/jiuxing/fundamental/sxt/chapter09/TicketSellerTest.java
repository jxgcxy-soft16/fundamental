/**
 * Project Name:fundamental
 * File Name:TicketSellerTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月21日下午2:03:54
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TicketSellerTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月21日下午2:03:54
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * ClassName:TicketSellerTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月21日 下午2:03:54 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TicketSellerTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月21日 下午2:03:54 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TicketSellerTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		TicketSeller ts = new TicketSeller();
		Thread t1 = new Thread(ts, "窗口一");
		Thread t2 = new Thread(ts, "窗口二");
		Thread t3 = new Thread(ts, "窗口三");
		t1.start();
		t2.start();
		t3.start();
	}

}

class TicketSeller implements Runnable {

	/**
	 * 车票总数
	 */
	private /*volatile*/ int num = 100;
//	private final AtomicInteger NUM2 = new AtomicInteger(100);
	
	private Object obj = new Object();
	@Override
	public void run() {
		while(true){
//			synchronized(obj){
				if(this.num<=0){
					break;
				}
				System.out.println(Thread.currentThread().getName()+"出售第"+num+"张票");
				this.num--;
//			}
			/*try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
		}
	}
	
}