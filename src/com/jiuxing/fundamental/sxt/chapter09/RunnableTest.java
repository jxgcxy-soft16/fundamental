/**
 * Project Name:fundamental
 * File Name:RunnableTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日上午9:09:38
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:RunnableTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日上午9:09:38
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:RunnableTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月19日 上午9:09:38 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: RunnableTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月19日 上午9:09:38 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class RunnableTest implements Runnable {

	private String name = "anynamous";
	
	public RunnableTest(String name){
		this.name = name;
	}
	
	public RunnableTest(){
		this("anynamous");
		return ;
	}
	/**
	 * TODO 简单描述该方法的实现功能（可选）.
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		for(int i=0;i<10;i++){
			System.out.println(this.name+" :  "+i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		}
	}

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		RunnableTest rt = new RunnableTest("线程一");
		RunnableTest rt2 = new RunnableTest("线程二");
		Thread t = new Thread(rt);
		Thread t2 = new Thread(rt2);
		Thread t3 = new Thread(()->{System.out.println("线程三");});
		System.out.println("monitor:线程一 isAlive="+t.isAlive());
		System.out.println("monitor:线程二 isAlive="+t2.isAlive());
		System.out.println("monitor:线程三 isAlive="+t3.isAlive());
		Thread t4 = new Thread(new Runnable(){
			public void run(){
				while(true){
					System.out.println("monitor:线程一 isAlive="+t.isAlive());
					System.out.println("monitor:线程二 isAlive="+t2.isAlive());
					System.out.println("monitor:线程三 isAlive="+t3.isAlive());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
			}
		});
		
		t4.start();
		
		t.start();
		t2.start();
		t3.start();
	}

}

