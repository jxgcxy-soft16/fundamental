/**
 * Project Name:fundamental
 * File Name:VolatileTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月21日下午10:27:48
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09;

import java.util.concurrent.TimeUnit;

/**
 * ClassName:VolatileTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月21日 下午10:27:48 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class VolatileTest {

	/**
	 * volatile 保证线程之间的数据可见性,但不保证准确性
	 */
	 volatile boolean running = true;
	void m(){
		System.out.println("m start");
		while(running){
			/*try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
		}
		System.out.println("m end");
	}
	public static void main(String[] args) {
		VolatileTest vt = new VolatileTest();
		new Thread(vt::m, "t1").start();
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		vt.running = false;
	}

}

