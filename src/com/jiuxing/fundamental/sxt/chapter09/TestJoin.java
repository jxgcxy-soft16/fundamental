/**
 * Project Name:fundamental
 * File Name:TestJoin.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2018年1月1日下午5:35:54
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TestJoin.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2018年1月1日下午5:35:54
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:TestJoin <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月1日 下午5:35:54 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TestJoin <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月1日 下午5:35:54 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TestJoin {

	public static void main(String[] args) {
		Runnable2 runnable = new Runnable2();
		Runnable2 runnable2 = new Runnable2();
		Thread thread = new Thread(runnable);
		thread.setName("runnable");
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Thread thread2 = new Thread(runnable2);
		thread2.setName("runnable2");
		thread2.start();
		
		for(int i = 0 ; i<10; i++){
			System.out.println("Main....."+i);
		}
	}
}
class Runnable2 implements Runnable{

	public void run() {
		for(int i = 0 ; i<10; i++){
			System.out.println(Thread.currentThread().getName()+"....."+i);
		}
	}
}
