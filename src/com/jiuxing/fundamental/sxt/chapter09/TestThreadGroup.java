/**
 * Project Name:fundamental
 * File Name:TestThreadGroup.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月20日上午9:33:13
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TestThreadGroup.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月20日上午9:33:13
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:TestThreadGroup <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月20日 上午9:33:13 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TestThreadGroup <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月20日 上午9:33:13 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TestThreadGroup {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		ThreadGroup tg = new ThreadGroup("TG");
		T1 t1 = new T1();
		Thread tt1 = new Thread(tg, t1);
//		tt1.setPriority(Thread.NORM_PRIORITY+3);
		T2 t2 = new T2();
		Thread tt2 = new Thread(tg, t2);
		tt2.setPriority(Thread.MAX_PRIORITY);
		
		tt1.start();
		tt2.start();
	}

}
class T1 implements Runnable {
	@Override public void run(){
		for(int i=0;i<200;i++){
			System.out.println("T1, i="+i);
		}
	}
}

class T2 implements Runnable {
	@Override public void run(){
		for(int i=0;i<200;i++){
			System.out.println("T2, i="+i);
		}
	}
}

