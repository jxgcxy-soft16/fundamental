/**
 * Project Name:fundamental
 * File Name:Consumer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日下午11:31:15
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Consumer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日下午11:31:15
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName:Consumer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月19日 下午11:31:15 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Consumer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月19日 下午11:31:15 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Consumer extends Thread {

	private Producer producer;
	
	public Consumer(String name, Producer producer){
		super(name);
		this.producer = producer;
	}
	
	@Override public void run(){
		while(true){
			Message msg = producer.waitMsg();
			System.out.println("consumer "+getName()+" get a msg");
		}
	}
	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Producer producer = new Producer();
		producer.start();
		
		Consumer c1 = new Consumer("消费者一",producer);
		Consumer c2 = new Consumer("消费者二",producer);
		Consumer c3 = new Consumer("消费者三",producer);
		c1.start();
		c2.start();
		c3.start();
	}
	

}

class Message {
	
}

class Producer extends Thread {
	List<Message> msgList = new ArrayList<Message>();
	
	@Override public void run(){
		try {
			while(true){
				Thread.sleep(3000);
				Message msg = new Message();
				synchronized(msgList){
					msgList.add(msg);
					msgList.notify();
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public Message waitMsg(){
		synchronized(msgList){
			if(msgList.size()==0){
				try {
					msgList.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return msgList.remove(0);
		}
	}
}

