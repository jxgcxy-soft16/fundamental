/**
 * Project Name:fundamental
 * File Name:SynchTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月20日上午10:49:27
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:SynchTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月20日上午10:49:27
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:SynchTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月20日 上午10:49:27 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: SynchTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月20日 上午10:49:27 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class SynchTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		S s = new S();
		new Thread(new C1(s), "C1").start();
		new Thread(new C2(s), "C2").start();
		new Thread(new C3(s), "C3").start();
	}

}
class S {
	public void say(String msg){
		System.out.println("S say "+Thread.currentThread().getName());
		echo(msg);
	}
	
	private synchronized void echo(String msg){
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("S echo:"+msg);
	}
}
class C1 implements Runnable {
	
	private S s;
	public C1( S s){
		this.s = s;
	}

	@Override
	public void run() {
		while(true){
			s.say("C1");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class C2 implements Runnable {
	
	private S s;
	public C2( S s){
		this.s = s;
	}

	@Override
	public void run() {
		while(true){
			s.say("C2");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class C3 implements Runnable {
	
	private S s;
	public C3( S s){
		this.s = s;
	}

	@Override
	public void run() {
		while(true){
			s.say("C3");
			try {
				Thread.sleep(800);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}


