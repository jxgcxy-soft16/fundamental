/**
 * Project Name:fundamental
 * File Name:WaitTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月20日上午10:30:50
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:WaitTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月20日 上午10:30:50 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class WaitTest {

	public static void main(String[] args) {
		Object obj = new Object();
		Waiter w = new Waiter("Waiter", obj);
		w.start();
		
		Notifyer n = new Notifyer("Notifyer", obj);
		n.start();
	}

}

class Waiter extends Thread {
	private Object obj;
	public Waiter(String name, Object obj){
		super(name);
		this.obj = obj;
	}
	@Override public void run(){
		int count = 0;
		while(true){
			try {
				synchronized(obj){
					System.out.println(this.getName()+": obj is to wait ");
//					sleep(2000);
					obj.wait();
				}
				
				count++;
				System.out.println(this.getName()+": obj is notified at "+count+" times");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
}

class Notifyer extends Thread {
	
	private Object obj;
	public Notifyer(String name, Object obj){
		super(name);
		this.obj = obj;
	}
	
	@Override public void run(){
		while(true){
			try {
				synchronized(obj){
					obj.notify();
					sleep(5000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
}

