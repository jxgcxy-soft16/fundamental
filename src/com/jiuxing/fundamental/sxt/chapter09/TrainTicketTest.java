/**
 * Project Name:fundamental
 * File Name:TrainTicketTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月22日上午8:38:30
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TrainTicketTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月22日上午8:38:30
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09;

import java.util.concurrent.TimeUnit;

/**
 * ClassName:TrainTicketTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月22日 上午8:38:30 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TrainTicketTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月22日 上午8:38:30 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TrainTicketTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		TrainTicketWindow ttw = new TrainTicketWindow();
		new Thread(ttw,"窗口一").start();
		new Thread(ttw,"窗口二").start();
		new Thread(ttw,"窗口三").start();
	}

}

class TrainTicketWindow implements Runnable {
	/**
	 * 总票数
	 */
	private /*volatile*/ int ticketTotal = 10;
	
	@Override public  void run(){
		while(true){
			synchronized(this){
				
				if(ticketTotal==0){
					return;
				}
				System.out.println(Thread.currentThread().getName()+"卖出第"+this.ticketTotal+"张车票");
				ticketTotal = ticketTotal - 1;
			}
			
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

