/**
 * Project Name:fundamental
 * File Name:AccountTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09.execise
 * Date:2017年12月23日下午1:10:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09.execise;

import java.util.concurrent.TimeUnit;

/**
 * ClassName:AccountTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月23日 下午1:10:28 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class AccountTest {
	
	private final Account account = new Account();
	
	public void save(){
		while(true){
			synchronized(account){
				while(account.balance>=500){
					try {
						account.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				final int amount = 2000;
				System.out.println(Thread.currentThread().getName()+" 存入 "+amount);
				account.balance += amount;
				showBalance();
				account.notifyAll();
			}
			try {
				TimeUnit.SECONDS.sleep(9);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void withdraw(){
		while(true){
			synchronized(account){
				if(account.balance<500){
					try {
						account.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				final int amount = 500;
				System.out.println(Thread.currentThread().getName()+" 取出 "+amount);
				account.balance -= amount;
				showBalance();
			}
			
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void showBalance(){
		System.out.println("当前余额:"+account.balance);
		System.out.println("---------------------------------");
	}

	public static void main(String[] args) {
		AccountTest at = new AccountTest();
		at.showBalance();
		new Thread(at::save, "家长").start(); 
		new Thread(at::withdraw, "学生").start();
	}

}

class Account {
	int balance;
}

