/**
 * Project Name:fundamental
 * File Name:BankAcountTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09.execise
 * Date:2017年12月28日上午8:46:16
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:BankAcountTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09.execise
 * Date:2017年12月28日上午8:46:16
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter09.execise;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ClassName:BankAcountTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月28日 上午8:46:16 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: BankAcountTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月28日 上午8:46:16 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class BankAcountTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		BankAccount ba = new BankAccount();
		Runnable p = new Parent(ba);
		Runnable c = new Child(ba);
		new Thread(p, "家长").start();
		new Thread(c, "学生").start();
	}

}
class BankAccount {
	/**
	 * 账户余额
	 */
	private int balance = 0;
	private Lock lock = new ReentrantLock();
	private Condition ca = lock.newCondition();
	private Condition cb = lock.newCondition();
	
	public void save(){
		/*synchronized(this){*/
		try {
			lock.lock();
			while(balance<500){
				balance += 2000;
				System.out.println("家长存入2000");
				System.out.println("账户余额"+balance);
//				this.notifyAll();
				ca.signalAll();
			}
		}
		finally {
			lock.unlock();
		}
		
		/*}*/
			
	}
	
	public void withdraw(){
		/*synchronized(this){*/
		try {
			lock.lock();
			while(balance<500){
				try {
//					this.wait();
					ca.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			balance -= 500;
			System.out.println("学生取出500");
			System.out.println("账户余额"+balance);
		}
		finally {
			lock.unlock();
		}
		
		
			/*}*/
	}
}
class Parent implements Runnable {

	/**
	 * 学生账户
	 */
	private BankAccount bankAccount;
	
	public Parent(BankAccount bankAccount){
		this.bankAccount = bankAccount;
	}
	@Override
	public void run() {
		while(true){
			this.bankAccount.save();
			try {
				TimeUnit.SECONDS.sleep(8);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}

class Child implements Runnable {

	/**
	 * 学生账户
	 */
	private BankAccount bankAccount;
	
	public Child(BankAccount bankAccount){
		this.bankAccount = bankAccount;
	}
	@Override
	public void run() {
		while(true){
			this.bankAccount.withdraw();
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}

