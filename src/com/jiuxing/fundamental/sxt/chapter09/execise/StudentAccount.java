/**
 * Project Name:fundamental
 * File Name:StudentAccount.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09.execise
 * Date:2017年12月28日上午11:17:15
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09.execise;

import java.util.concurrent.TimeUnit;

/**
 * ClassName:StudentAccount <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月28日 上午11:17:15 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class StudentAccount {
	

	public static void main(String[] args) {
		StudentAccount sa = new StudentAccount();
		new Thread(()->sa.deposit(), "家长1").start();
		new Thread(()->sa.deposit(), "家长2").start();
		new Thread(sa::withdraw, "学生").start();
	}
	
	public void deposit(){
		while(true){
			synchronized(this){
				while(balance>=2000){
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				this.balance += 2000;
				System.out.println(Thread.currentThread().getName()+"存入"+2000);
				System.out.println("账户余额"+this.balance);
				this.notifyAll();
			}
			try {
				TimeUnit.SECONDS.sleep(6);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void withdraw(){
		while(true){
			synchronized(this){
				while(this.balance<500){
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				this.balance -= 500;
				System.out.println(Thread.currentThread().getName()+"取出"+500);
				System.out.println("账户余额"+this.balance);
				this.notifyAll();
			}
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private int balance = 0;

}

