/**
 * Project Name:fundamental
 * File Name:AccountTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09.execise
 * Date:2017年12月23日下午1:10:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09.execise;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ClassName:AccountTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月23日 下午1:10:28 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class AccountTest2 {
	
	private int balance = 0;
	private Lock lock = new ReentrantLock(true);
	private Condition lessCond = lock.newCondition();
	private Condition moreCond = lock.newCondition();
	
	public void save(){
		while(true){
			lock.lock();
			try {
				final int amount = 2000;
				while(this.balance>500){
					try {
						lessCond.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName()+" 存入 "+amount);
				this.balance += amount;
				showBalance();
				moreCond.signalAll();
			}
			finally{
				lock.unlock();
			}
			/*synchronized(this){
				final int amount = 2000;
				while(this.balance>500){
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName()+" 存入 "+amount);
				this.balance += amount;
				showBalance();
				this.notifyAll();
			}*/
/*			try {
				TimeUnit.SECONDS.sleep(9);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
*/		}
	}
	
	public void withdraw(){
		while(true){
			try {
				lock.lock();
				while(this.balance==0){
					try {
						moreCond.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				final int amount = 500;
				System.out.println(Thread.currentThread().getName()+" 取出 "+amount);
				this.balance -= amount;
				showBalance();
				lessCond.signalAll();
			}
			finally{
				lock.unlock();
			}
			/*synchronized(this){
				while(this.balance==0){
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				final int amount = 500;
				System.out.println(Thread.currentThread().getName()+" 取出 "+amount);
				this.balance -= amount;
				showBalance();
				this.notifyAll();
			}*/
			
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void showBalance(){
		System.out.println("当前余额:"+this.balance);
		System.out.println("---------------------------------");
	}

	public static void main(String[] args) {
		AccountTest2 at = new AccountTest2();
		at.showBalance();
		new Thread(at::save, "家长1").start(); 
		new Thread(at::save, "家长2").start(); 
		new Thread(at::withdraw, "学生1").start();
		new Thread(at::withdraw, "学生2").start();
	}

}


