/**
 * Project Name:fundamental
 * File Name:Thread.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter09
 * Date:2017年12月19日上午8:46:33
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter09;
/**
 * ClassName:Thread <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月19日 上午8:46:33 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ThreadTest extends Thread {

	private int count = 0; 
	public static void main(String[] args){
		ThreadTest t = new ThreadTest("one");
		ThreadTest t2 = new ThreadTest("two");
		t.start();
		t2.start();
		
	}
	
	public ThreadTest(String name){
		this.setName(name);
	}
	
	@Override
	public void run(){
		
		while(true){
			/*if(count>20){
				break;
			}
			count++;*/
			System.out.println("线程["+this.getName()+"]测试第"+count+"次");
			/*try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			yield();
		}
		
	}
}

