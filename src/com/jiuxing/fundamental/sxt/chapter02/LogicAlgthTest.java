/**
 * Project Name:fundamental
 * File Name:LogicAlgthTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter02
 * Date:2018年1月8日上午11:34:02
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter02;
/**
 * ClassName:LogicAlgthTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月8日 上午11:34:02 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class LogicAlgthTest {

	public static void main(String[] args) {
		LogicAlgthTest lat = new LogicAlgthTest();
		boolean c = lat.a()&&lat.b();
	}
	
	public boolean a(){
		System.out.println("method a");
		return false;
	}
	
	public boolean b(){
		System.out.println("method b");
		return true;
	}

}

