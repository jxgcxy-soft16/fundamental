/**
 * Project Name:fundamental
 * File Name:VarTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter02
 * Date:2018年1月8日上午9:07:05
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter02;
/**
 * ClassName:VarTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月8日 上午9:07:05 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class VarTest {

	public static void main(String[] args) {
		int i;
//		System.out.println(i);
		VarTest vt = new VarTest();
		System.out.println(vt.j);
	}

	private int j;
}

