/**
 * Project Name:fundamental
 * File Name:VarTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter02
 * Date:2018年1月8日上午9:07:05
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter02;
/**
 * ClassName:VarTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月8日 上午9:07:05 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class VarTest3 {

	public static void main(String[] args) {
		new VarTest3().method();
	}

	public void method(){
		int i=1,j=0;
		float f1=0.1f; float f2=123;
		long l1=12345678,l2=8888888888L;
		double d1=2e20,d2=124;
		byte b1=1,b2=2,b3=127;
		j=j+10;
		i=i/10;
		i=(int)(i*0.1);
		char c1='a',c2=125;
		byte b = (byte)(b1-b2);
		char c = (char)(c1+c2-1);
		char c3 = 'a'+125-1;
		float f3 = f1+f2;
		float f4 = f1+(float)(f2*0.1);
		double d = d1*i+j;
		float f = (float)(d1*5+d2);
		System.out.println("f="+f);
		
	}
}

