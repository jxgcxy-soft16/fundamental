/**
 * Project Name:fundamental
 * File Name:VarTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter02
 * Date:2018年1月8日上午9:07:05
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter02;
/**
 * ClassName:VarTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月8日 上午9:07:05 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class VarTest2 {

	public static void main(String[] args) {
		byte i = 2;
		short j = i;
		System.out.println(j);
		
		short m = 3;
		byte n = (byte)m;
		System.out.println("n="+n);
		
		short a = 64;
		char c = (char)a;
		System.out.println("c="+c);
		
		byte x1=2;
		short x2 = 3;
		char x7 = 'a';
		int x3 = 4;
		long x4 = 5;
		float x5 = 3.14f;
		double x6 = 2.3;
		System.out.println("sum="+(x1+x2+x3+x4+x5+x6+x7));
	}

}

