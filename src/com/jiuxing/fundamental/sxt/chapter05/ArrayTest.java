/**
 * Project Name:fundamental
 * File Name:ArrayTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter05
 * Date:2017年11月23日上午9:00:33
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ArrayTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter05
 * Date:2017年11月23日上午9:00:33
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter05;
/**
 * ClassName:ArrayTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月23日 上午9:00:33 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ArrayTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月23日 上午9:00:33 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ArrayTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		/**
		 * 静态初始化
		 */
		int [][] a = {{23,4,8},{3},{8,12}};
		
		/**
		 * 动态初始化
		 */
		int [][] b = new int[2][];
		b[0] = new int[3];
		b[0][0] = 2;
		b[0][1] = 23;
		b[0][2] = 50;
		b[1] = new int[2];
		b[1][0] = 67;
		b[1][1] = 89;
		
		int[][] c = new int[0][];
		
		int[][] d = {{1},{2}};
		
		System.out.println("array b print following:");
		for(int i=0;i<b.length;i++){
			for(int j=0;j<b[i].length;j++){
				System.out.print(b[i][j]+" ");
			}
			System.out.println();
		}
		
		String[][] e;
		e = new String[3][];
		e[0] = new String[2];
		e[0][0] = "alice";
		e[0][1] = "uk";
		
		e[1] = new String[]{"peter","us","wanshtong"};
		
		e[2] = new String[]{"sdfds","ddf", "uiioo","fwfw"};
		
		System.out.println("array e print following:");
		for(String[] i:e){
			for(String j:i){
				System.out.print(j+" ");
			}
			System.out.println();
		}

	}

}

