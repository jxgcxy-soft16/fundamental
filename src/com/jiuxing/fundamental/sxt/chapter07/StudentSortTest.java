/**
 * Project Name:fundamental
 * File Name:StudentSortTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月12日下午3:31:56
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:StudentSortTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月12日下午3:31:56
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * ClassName:StudentSortTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月12日 下午3:31:56 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: StudentSortTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月12日 下午3:31:56 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class StudentSortTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		Set<Student> set = new TreeSet<Student>((x,y)->x.getStuno()-y.getStuno());
		set.add(new Student(1000,"丁一",80,72.5,60));
		set.add(new Student(1003,"李四",80,80,88));
		set.add(new Student(1004,"景厅",60,92,98));
		set.add(new Student(1005,"周六",50,30,56));
		set.add(new Student(1001,"王二月",77,45,69));
		set.add(new Student(1002,"张三",60,60,40));
		
		
		System.out.println("按学号排序:");
		for(Iterator<Student> it = set.iterator();it.hasNext();){
			Student s = it.next();
			System.out.println(s);
		}
		
		Set<Student> setTotal = new TreeSet<Student>(new TotalCmp());
		setTotal.addAll(set);
		System.out.println("按总分排序:");
		for(Iterator<Student> it = setTotal.iterator();it.hasNext();){
			Student s = it.next();
			System.out.println(s);
		}
		
		Set<Student> setChinse = new TreeSet<Student>(new ChineseCmp());
		setChinse.addAll(set);
		System.out.println("按语文分排序:");
		for(Iterator<Student> it = setChinse.iterator();it.hasNext();){
			Student s = it.next();
			System.out.println(s);
		}
		
		Set<Student> setMath = new TreeSet<Student>(new MathCmp());
		setMath.addAll(set);
		System.out.println("按数学分排序:");
		for(Iterator<Student> it = setMath.iterator();it.hasNext();){
			Student s = it.next();
			System.out.println(s);
		}
		
		Set<Student> setEnglish = new TreeSet<Student>(new EnglishCmp());
		setEnglish.addAll(set);
		System.out.println("按英语分排序:");
		for(Iterator<Student> it = setEnglish.iterator();it.hasNext();){
			Student s = it.next();
			System.out.println(s);
		}
	}

}

class Student {
	/**
	 * 学号
	 */
	private int stuno;
	
	/**
	 * 名字
	 */
	private String name;
	
	/**
	 * 语文分
	 */
	private double chinese;
	
	/**
	 * 数学分
	 */
	private double math;
	
	/**
	 * 英语分
	 */
	private double english;
	
	
	public Student(int stuno, String name, double chinese, double math, double english) {
		super();
		this.stuno = stuno;
		this.name = name;
		this.chinese = chinese;
		this.math = math;
		this.english = english;
	}
	
	public int getStuno() {
		return stuno;
	}
	public void setStuno(int stuno) {
		this.stuno = stuno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getChinese() {
		return chinese;
	}
	public void setChinese(double chinese) {
		this.chinese = chinese;
	}
	public double getMath() {
		return math;
	}
	public void setMath(double math) {
		this.math = math;
	}
	public double getEnglish() {
		return english;
	}
	public void setEnglish(double english) {
		this.english = english;
	}
	
	public double getAvg(){
		return this.getTotal()/3;
	}
	
	public double getTotal(){
		return (this.chinese+this.math+this.english);
	}
	
	@Override
	public int hashCode(){
		return this.stuno;
	}
	
	@Override
	public boolean equals(Object obj){
		if(null == obj
				||this.stuno==0
				||null == this.name){
			return false;
		}
		if(obj instanceof Student){
			Student stu = (Student)obj;
			return this.stuno == stu.stuno;
		}
		return false;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("{stuno").append(":").append(this.getStuno())
		.append(",").append("name").append(":").append(this.getName())
		.append(",").append("chinese").append(":").append(this.getChinese())
		.append(",").append("math").append(":").append(this.getMath())
		.append(",").append("english").append(":").append(this.getEnglish())
		.append(",").append("total").append(":").append(this.getTotal())
		.append(",").append("avg").append(":").append(this.getAvg())
		.append("}");
		return sb.toString();
	}
}

class StunoCmp implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getStuno()>o2.getStuno()){
			return 1;
		} else if(o1.getStuno()<o2.getStuno()){
			return -1;
		}
		return 0;
	}
	
}

class ChineseCmp extends StunoCmp {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getChinese()>o2.getChinese()){
			return 1;
		} else if(o1.getChinese()<o2.getChinese()){
			return -1;
		}
		return super.compare(o1, o2);
	}
	
}

class MathCmp implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getMath()>o2.getMath()){
			return 1;
		} else if(o1.getMath()<o2.getMath()){
			return -1;
		}
		return 0;
	}
}

class EnglishCmp implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getEnglish()>o2.getEnglish()){
			return 1;
		} else if(o1.getEnglish()<o2.getEnglish()){
			return -1;
		}
	
		return 0;
	}
}

class TotalCmp implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getTotal()>o2.getTotal()){
			return 1;
		} else if(o1.getTotal()<o2.getTotal()){
			return -1;
		}
		return 0;
	}
}

class AvgCmp implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getAvg()>o2.getAvg()){
			return 1;
		} else if(o1.getAvg()<o2.getAvg()){
			return -1;
		}
		return 0;
	}
}
