/**
 * Project Name:fundamental
 * File Name:ArrayListTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午9:10:22
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ArrayListTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午9:10:22
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * ClassName:ArrayListTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月5日 上午9:10:22 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ArrayListTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月5日 上午9:10:22 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ArrayListTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		Collection c = new ArrayList();
		c.add("hello");
		c.add(new Name("f1","l1"));
		c.add(new Integer(100));
		Collection c2 = Arrays.asList("ab","cd","ef");
		c.addAll(c2);
		System.out.println(c.size());
		System.out.println(c);
//		c.forEach(System.out::print);
	}

}

