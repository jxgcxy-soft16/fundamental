/**
 * Project Name:fundamental
 * File Name:ComparableTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月6日下午5:11:09
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ComparableTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月6日下午5:11:09
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * ClassName:ComparableTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月6日 下午5:11:09 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ComparableTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月6日 下午5:11:09 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ComparableTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		List l1 = new LinkedList();
		l1.add(new Name("Karl","M"));
		l1.add(new Name("Steve","Lee"));
		l1.add(new Name("John","O"));
		l1.add(new Name("Tom","M"));
		
		System.out.println(l1);
		Collections.sort(l1);
		System.out.println(l1);
	}

}

