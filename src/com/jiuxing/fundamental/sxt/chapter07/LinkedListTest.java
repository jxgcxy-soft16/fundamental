/**
 * Project Name:fundamental
 * File Name:LinkedListTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午10:36:18
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:LinkedListTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午10:36:18
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.Collection;
import java.util.LinkedList;

/**
 * ClassName:LinkedListTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月5日 上午10:36:18 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: LinkedListTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月5日 上午10:36:18 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class LinkedListTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Collection c = new LinkedList();
		c.add(new Name("f1","l1"));
		c.add(new Name("f2","l2"));
		System.out.println(c.contains(new Name("f2","l2")));
		c.remove(new Name("f1","l1"));
		System.out.println(c);
		// TODO Auto-generated method stub

	}

}

