/**
 * Project Name:fundamental
 * File Name:Name.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午9:20:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Name.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午9:20:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;
/**
 * ClassName:Name <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月5日 上午9:20:43 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Name <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月5日 上午9:20:43 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Name implements Comparable {

	private String firstName,lastName;

	public Name(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String toString(){
		return new StringBuilder(this.firstName)
				.append(" ").append(this.lastName).toString();
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj==null
				||null==this.firstName
				||null==this.lastName){
			return false;
		}
		if(obj instanceof Name){
			Name n = (Name)obj;
			return this.firstName.equals(n.getFirstName())
					&&this.lastName.equals(n.getLastName());
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		return this.firstName.hashCode()*31+this.lastName.hashCode();
	}

	@Override
	public int compareTo(Object o) {
		if(null==o){
			return 0;
		}
		if(o instanceof Name){
			Name n = (Name)o;
			int cmp = this.lastName.compareTo(n.getLastName());
			if(cmp==0){
				cmp = this.firstName.compareTo(n.getFirstName());
			}
			return cmp;
		}
		return 0;
	}
}

