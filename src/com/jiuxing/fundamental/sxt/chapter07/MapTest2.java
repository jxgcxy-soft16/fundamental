/**
 * Project Name:fundamental
 * File Name:MapTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月7日上午9:26:21
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName:MapTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月7日 上午9:26:21 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class MapTest2 {

	private static final int ONE = 1;
	public static void main(String[] args) {
		Map m = new HashMap();
		m.put("one", 1);
		m.put("two", new Integer(2));
		for(int i=0;i<args.length;i++){
			Integer freq = (Integer)m.get(args[i]);
			m.put(args[i], (freq==null?ONE:freq+1));
		}
		int two = (Integer)m.get("two")+1;
		System.out.println("two="+two);
		System.out.println(m.size()+" distinct words detected:");
		System.out.println(m);
	}

}

