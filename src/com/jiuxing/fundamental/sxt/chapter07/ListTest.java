/**
 * Project Name:fundamental
 * File Name:ListTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月6日下午4:18:49
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ListTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月6日下午4:18:49
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.LinkedList;
import java.util.List;

/**
 * ClassName:ListTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月6日 下午4:18:49 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ListTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月6日 下午4:18:49 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ListTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		List list = new LinkedList();
		for(int i=0;i<=5;i++){
			list.add("a"+i);
		}
		System.out.println(list);
		list.add(3, "a100");
		System.out.println(list);
		list.set(6, "a200");
		System.out.println(list);
		System.out.print((String)list.get(2)+" ");
		System.out.println(list.indexOf("a3"));
		list.remove(1);
		System.out.println(list);
		
		
		
	}

}

