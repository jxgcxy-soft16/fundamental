/**
 * Project Name:fundamental
 * File Name:MapTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月7日上午8:57:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MapTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月7日上午8:57:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * ClassName:MapTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月7日 上午8:57:43 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MapTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月7日 上午8:57:43 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class MapTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		Map m1 = new HashMap();
		Map m2 = new TreeMap();
		m1.put("one", 1.2);
		m1.put("two", new Double(2.78));
		m1.put("three", new Double(3.14));
		m2.put("A", new Double(1.23));
		m2.put("two", new Double(2.889));
		m2.put("C", new Double(1.889));
		m2.put("alice", new Double(0.21));
		System.out.println(m1.size());
		System.out.println(m1.containsKey("one"));
		System.out.println(m2.containsValue(new Double(3.12)));
		if(m1.containsKey("two")){
			double i = ((Double)m1.get("two")).doubleValue();
			System.out.println(i);
		}
		Map m3 = new HashMap(m1);
		m3.putAll(m2);
		System.out.println(m3);
		System.out.println("m2="+m2);
		System.out.println("1+2="+(1+new Integer(2)));
		
	}

}

