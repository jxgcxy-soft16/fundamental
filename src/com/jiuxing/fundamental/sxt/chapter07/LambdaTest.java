/**
 * Project Name:fundamental
 * File Name:LambdaTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月13日上午8:44:10
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:LambdaTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月13日上午8:44:10
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;
/**
 * ClassName:LambdaTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月13日 上午8:44:10 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: LambdaTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月13日 上午8:44:10 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class LambdaTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Welcome welcome = (msg)->
		System.out.println(msg);
		
		//匿名类
		Welcome welcome2 = new Welcome(){

			@Override
			public void say(String msg) {
				System.out.println("匿名类:"+msg);
			}
		};
		
		welcome.say("hello");
		welcome2.say("skdldsk");
		
		Operator add = ( x,  y)->{return x+y;};
		Operator sub = (int x, int y)->x-y;
		Operator multi = (int x, int y)->x*y;
		Operator div = (int x, int y)->x/y;
		
		int x=5,y=7;
		System.out.println(x+"+"+y+"="+add.operate(x, y));
		System.out.println(x+"-"+y+"="+sub.operate(x, y));
		System.out.println(x+"*"+y+"="+multi.operate(x, y));
		System.out.println(x+"/"+y+"="+div.operate(x, y));
	}

}

interface Welcome {
	void say(String msg);
}

interface Operator {
	int operate(int x, int y);
//	int b(int x,int y);
}

