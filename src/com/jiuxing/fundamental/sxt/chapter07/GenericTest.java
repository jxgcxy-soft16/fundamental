/**
 * Project Name:fundamental
 * File Name:GenericTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月8日上午8:46:03
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:GenericTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月8日上午8:46:03
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * ClassName:GenericTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月8日 上午8:46:03 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: GenericTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月8日 上午8:46:03 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class GenericTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		List<String> l1 = new ArrayList<String>();
		l1.add("aaa");
		l1.add("bbb");
		l1.add("ccc");
		System.out.println("iterate as following:");
		for(Iterator<String> i = l1.iterator();i.hasNext();){
			String s = i.next();
			System.out.print(s+" ");
		}
		System.out.println("\nloop as following:");
		for(int i=0;i<l1.size();i++){
			String s = l1.get(i);
			System.out.print(s+" ");
		}
		
		Set<Name> s1 = new HashSet<Name>();
		s1.add(new Name("f1","l1"));
		s1.add(new Name("f2","l2"));
		s1.add(new Name("f11","l11"));
//		s1.add("abc");
		
		System.out.println("\nset's elements:");
		for(Iterator<Name> i = s1.iterator();i.hasNext();){
			Name n = i.next();
			System.out.print(n+",");
		}
		
		
		MyContainer<Integer> mc = new MyContainer<Integer>();
		mc.add(10);
		mc.add(20);
		mc.add(30);
		mc.add(40);
		
		System.out.println("mc="+mc);
	}

}
class MyContainer<A> {
	private List<A> list = new ArrayList<>();
	public A get(int index){
		return list.get(index);
	}
	
	public  void add(A a){
		list.add(a);
	}
	
	public int size(){
		return list.size();
	}
	
	@Override
	public String toString(){
//		Collection<A> c = list;
		return list.toString();
	}
}

