/**
 * Project Name:fundamental
 * File Name:IteratorTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午11:30:11
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:IteratorTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月5日上午11:30:11
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * ClassName:IteratorTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月5日 上午11:30:11 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: IteratorTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月5日 上午11:30:11 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class IteratorTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Collection c = new HashSet();
		c.add(new Name("fff1","lll1"));
		c.add(new Name("f2","l2"));
		c.add(new Name("fff3","lll3"));
		
//		c.remove(new Name("f2","l2"));
		c.add(new Name("f4","l4"));
		
		

		for(Iterator iterator = c.iterator();iterator.hasNext();){
			Name n = (Name)iterator.next();
			if(n.getFirstName().length()<3){
//				iterator.remove();
				c.remove(n);
			}
//			System.out.print(n.getFirstName()+" ");
		}
		System.out.println(c);
	}

}

