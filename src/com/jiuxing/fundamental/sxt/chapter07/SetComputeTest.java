/**
 * Project Name:fundamental
 * File Name:SetComputeTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月6日下午3:23:10
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:SetComputeTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2017年12月6日下午3:23:10
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.HashSet;
import java.util.Set;

/**
 * ClassName:SetComputeTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月6日 下午3:23:10 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: SetComputeTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月6日 下午3:23:10 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class SetComputeTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
//	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {

		Set<Object> s1 = new HashSet<Object>();
		Set<Name> s2 = new HashSet<Name>();
		s1.add("a");s1.add("b");s1.add("c");
		s1.add(new Name("f1","l1"));s1.add(new Name("f2","l2"));
		
		
//		s2.add("d");s2.add("a");s2.add("b");
		s2.add(new Name("f1","l1"));s2.add(new Name("f11","l11"));
		
		Set sn = new HashSet(s1);
		sn.retainAll(s2);
		Set su = new HashSet(s1);
		su.addAll(s2);
		Set sd = new HashSet(s1);
		sd.removeAll(s2);
		
		System.out.println(sn);
		System.out.println(su);
		System.out.println(sd);
		
	}

}

