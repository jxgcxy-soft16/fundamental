/**
 * Project Name:fundamental
 * File Name:IteratorTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter07
 * Date:2018年1月9日下午2:47:26
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter07;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * ClassName:IteratorTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月9日 下午2:47:26 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class IteratorTest2 {

	public static void main(String[] args) {

		// TODO Auto-generated method stubo
		List<Week> weeks = Arrays.asList(Week.values());
		for(Iterator<Week> iter = weeks.iterator();iter.hasNext();){
			Week day = iter.next();
			System.out.println(day);
			for(Iterator<Week> iter2 = weeks.iterator();iter2.hasNext();){
				Week day2 = iter2.next();
				System.out.println(day2);
			}
		}

	}
	
	enum Week {
		SUN,MON,TUE,WED,THU,FRI,SAT;
	}

}

