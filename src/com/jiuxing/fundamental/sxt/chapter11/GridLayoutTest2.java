/**
 * Project Name:fundamental
 * File Name:GridLayoutTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午3:16:16
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:GridLayoutTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午3:16:16
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:GridLayoutTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 下午3:16:16 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: GridLayoutTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月3日 下午3:16:16 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class GridLayoutTest2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Frame f = new Frame("网格管理器");
		f.setLayout(new GridLayout(3,4));
		
		Button[] btns = new Button[10];
		for(int i=0;i<btns.length;i++){
			btns[i] = new Button("按钮"+i);
			f.add(btns[i]);
		}
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		f.pack();
		f.setVisible(true);

	}

}

