/**
 * Project Name:fundamental
 * File Name:Car.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月4日下午2:15:49
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.io.PrintStream;

/**
 * ClassName:Car <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月4日 下午2:15:49 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Car {
	
	private 方向盘 a;
	private 轮胎  b;
	private 发动机  c;
	private 变速器  d;
	private 刹车  e;
	
	private static PrintStream out = System.out;

	public static void main(String[] args) {
		Car car = new Car();
		car.a = new 方向盘(); 
	}
	
	private static class 方向盘 {
		{
			out.println("方向盘 ...");
		}
	}
	
	private class 轮胎 {

		 {
			a = new 方向盘();
			c = new 发动机();
			out.println("轮胎 ...");
		}
	}
	
	private class 发动机 {

		
	}
	
	private class 变速器 {

		
	}
	
	private class 刹车 {

		
	}

}

class Bike {
	Bike(){
		Car car = new Car();
//		发动机 a = car.new 发动机();
	}
}

