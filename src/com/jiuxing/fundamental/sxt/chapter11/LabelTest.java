/**
 * Project Name:fundamental
 * File Name:LabelTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月4日下午11:04:28
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.awt.Image;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 * ClassName:LabelTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月4日 下午11:04:28 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class LabelTest {

	public static void main(String[] args) {

		JFrame f = new JFrame("标签测试");
		URL url = f.getClass().getResource("/assets/image/redcat.jpg");
//		Image img2 = Toolkit.getDefaultToolkit().getImage(url);
		Icon icon = new ImageIcon(url);
		
		JLabel label = new JLabel("sdfds",icon,JLabel.LEFT);
		JTextArea ta = new JTextArea(20,30);
		String[] files = {"th.jpg","swordman.jpg","redcat.jpg","bluecat.jpg","longneck.jpg"};
		ImageIcon[] icons = new ImageIcon[files.length];
		for(int i=0;i<icons.length;i++){
			URL url2 = f.getClass().getResource("/assets/image/"+files[i]);
			icons[i] = new ImageIcon(url2);
			icons[i].setImage(icons[i].getImage().getScaledInstance(50,  
	                50, Image.SCALE_DEFAULT));
		}
		
		JComboBox<ImageIcon> combo = new JComboBox<ImageIcon>(icons);
		f.getContentPane().add(label,BorderLayout.SOUTH);
		f.getContentPane().add(ta);
		f.getContentPane().add(combo, BorderLayout.NORTH);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);

	}

}

