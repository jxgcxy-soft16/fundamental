/**
 * Project Name:fundamental
 * File Name:SwingTest1.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月2日上午10:44:10
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:SwingTest1.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月2日上午10:44:10
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * ClassName:SwingTest1 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月2日 上午10:44:10 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: SwingTest1 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月2日 上午10:44:10 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class SwingTest1 {

	private static JTextArea ta;
	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		JFrame f = new JFrame("JFrame test");
		
		JMenuBar menubar = createMenuBar();
		f.add(menubar,BorderLayout.NORTH);
		
		JTextArea editarea = createTextArea();
		f.add(editarea, BorderLayout.CENTER);
		SwingTest1.ta = editarea;
		
		f.pack();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

	private static JTextArea createTextArea() {
		
		// TODO Auto-generated method stub
		JTextArea ta = new JTextArea(20,70);
		return ta;
	}

	private static JMenuBar createMenuBar() {
		
		// TODO Auto-generated method stub
		JMenuBar menubar = new JMenuBar();
		JMenu fileMenu = creatFileMenu();//
		JMenu editMenu = createEditMenu();//new JMenu("编辑");
		JMenu formMenu = new JMenu("格式");
		JMenu viewMenu = new JMenu("查看");
		JMenu helpMenu = new JMenu("帮助");
		menubar.add(fileMenu);
		menubar.add(editMenu);
		menubar.add(formMenu);
		menubar.add(viewMenu);
		menubar.add(helpMenu);
		
		return menubar;
	}

	private static JMenu createEditMenu() {
		JMenu editMenu = new JMenu("编辑");
		JMenuItem cancelItem = new JMenuItem("撤销");
		JMenuItem tItem = new JMenuItem("剪贴");
		JMenuItem cpItem = new JMenuItem("复制");
		JMenuItem delItem = new JMenuItem("删除");
		JMenuItem pasteItem = new JMenuItem("粘贴");
		JMenuItem findItem = new JMenuItem("查找");
		JMenuItem replaceItem = new JMenuItem("替换");
		JMenuItem allItem = new JMenuItem("全选");
		JMenuItem dateItem = new JMenuItem("日期");
		
		editMenu.add(cancelItem);
		editMenu.addSeparator();
		editMenu.add(tItem);
		editMenu.add(cpItem);
		editMenu.add(pasteItem);
		editMenu.add(delItem);
		editMenu.addSeparator();
		editMenu.add(findItem);
		editMenu.add(replaceItem);
		editMenu.addSeparator();
		editMenu.add(allItem);
		editMenu.add(dateItem);
		return editMenu;
	}

	private static JMenu creatFileMenu() {
		JMenu fileMenu = new JMenu("文件");
		JMenuItem newItem = new JMenuItem("新建");
		JMenuItem openItem = new JMenuItem("打开");
		
		openItem.addActionListener((e)->{
			JFileChooser chooser = new JFileChooser();
		    FileNameExtensionFilter filter = new FileNameExtensionFilter(
		        "文本文件", "txt");
		    chooser.setFileFilter(filter);
		    int returnVal = chooser.showOpenDialog(fileMenu.getParent().getParent());
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
		      /* System.out.println("You chose to open this file: " +
		            chooser.getSelectedFile().getName());*/
		       File file = chooser.getSelectedFile();
		       BufferedReader br = null; 
		       try {
					br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
					String line = null;
					while((line = br.readLine())!=null){
						SwingTest1.ta.append(line);
						SwingTest1.ta.append("\r\n");
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					if(br!=null){
						try {
							br.close();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}
		    }

		});
		JMenuItem saveAsItem = new JMenuItem("另存为");
		JMenuItem psItem = new JMenuItem("页面设置");
		JMenuItem printItem = new JMenuItem("打印");
		JMenuItem exitItem = new JMenuItem("退出");
		
		fileMenu.add(newItem);
		fileMenu.add(openItem);
		fileMenu.add(saveAsItem);
		fileMenu.addSeparator();
		fileMenu.add(psItem);
		fileMenu.add(printItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);
		return fileMenu;
	}

}

