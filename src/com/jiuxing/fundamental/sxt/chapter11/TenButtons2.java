/**
 * Project Name:fundamental
 * File Name:TenButtons2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午4:13:14
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TenButtons2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午4:13:14
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:TenButtons2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 下午4:13:14 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TenButtons2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月3日 下午4:13:14 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TenButtons2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Frame f = new Frame("西湖十景");
		GridLayout gl = new GridLayout(2,1);
		BorderLayout bl = new BorderLayout();
		
		f.setLayout(gl);
		
		createUpPanel(f,bl);
		createDownPanel(f,bl);
//		f.add(new Button("first"));
//		f.add(new Button("second"));
//		f.add(pfu);
//		f.add(pfd);
		
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
//		f.pack();
		f.setBounds(20, 30, 400, 300);
		f.setVisible(true);
	}

	private static Panel createUpPanel(Frame f, BorderLayout bl) {
		Panel pfu = new Panel();
		pfu.setLayout(new BorderLayout());
		pfu.add(new Button("断桥残雪"), BorderLayout.EAST);
		pfu.add(new Button("三潭映月"), BorderLayout.WEST);
		
		Panel pfuc = new Panel();
		pfuc.setLayout(new GridLayout(2,1));
		pfuc.add(new Button("柳浪闻莺"));
		pfuc.add(new Button("苏堤春晓"));
		pfu.add(pfuc);
		
		f.add(pfu);
		return pfu;
	}

	private static Panel createDownPanel(Frame f, BorderLayout bl) {
		Panel pfd = new Panel();
		pfd.setLayout(new BorderLayout());
		pfd.add(new Button("曲院风荷"), BorderLayout.EAST);
		pfd.add(new Button("雷峰西照"), BorderLayout.WEST);
		
		Panel pfdc = new Panel();
		pfdc.setLayout(new GridLayout(2,2));
		pfdc.add(new Button("花港观鱼"));
		pfdc.add(new Button("平湖秋月"));
		pfdc.add(new Button("双峰插云"));
		pfdc.add(new Button("南屏晚钟"));
		
		pfd.add(pfdc);
		
		f.add(pfd);
		return pfd;
	}

}

