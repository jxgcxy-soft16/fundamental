/**
 * Project Name:fundamental
 * File Name:CenterPanel.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月2日上午9:42:23
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:CenterPanel <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月2日 上午9:42:23 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class CenterPanel {

	public static void main(String[] args) {
		java.awt.Frame frame = new java.awt.Frame("FrameWithPanel");
		int width = 400,height = 300;
		frame.setSize(width, height);
		frame.setLayout(null);
		Panel p = new Panel();
		p.setBackground(Color.YELLOW);
		frame.setBackground(Color.BLUE);
		frame.add(p);
		p.setBounds(width/4,height/4, width/2, height/2);
//		p.setSize(width/2, height/2);
//		frame.pack();
		
		frame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
			}
		});
		
		frame.setVisible(true);
	}

}

