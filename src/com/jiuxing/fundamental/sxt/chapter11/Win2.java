/**
 * Project Name:fundamental
 * File Name:Win2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2017年12月29日上午10:54:04
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Win2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2017年12月29日上午10:54:04
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:Win2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月29日 上午10:54:04 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Win2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月29日 上午10:54:04 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Win2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Frame f = new Frame("窗口二");
//		f.setLayout(new FlowLayout());
		Panel pn = new Panel();
		pn.setLayout(new FlowLayout(FlowLayout.LEFT));
		Button b1 = new Button("button1");
		pn.add(b1);
		
		Button b2 = new Button("button2");
		pn.add(b2);
		f.add(pn, "North");
		
		TextArea ta = new TextArea(10,30);
		f.add(ta, "Center");
		
		Panel ps = new Panel();
		
		TextField tf = new TextField(20);
		tf.setEchoChar('#');
		tf.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				System.out.println(e.getActionCommand());
				
			}
			
		});
		/*tf.addKeyListener(new KeyAdapter(){
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyChar()=='\r'||e.getKeyChar()=='\n'){
					ta.append(tf.getText());
					ta.append("\r\n");
					tf.setText("");
				}
			}
			
		});*/
		ps.add(tf);
		Button b3 = new Button("发送");
		/**
		 * 匿名内部类创建对象语法:
		 * 
		 * new [接口名|抽象类名](){
		 * 	//方法重写
		 * }
		 * 
		 */
		b3.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				ta.append(tf.getText());
				ta.append("\r\n");
				tf.setText("");
			}
		});
		b3.addActionListener(a->ta.append("lambjojoiiii:"+a.getActionCommand()));
//		BtnListener bl = new BtnListener(ta);
//		b3.addActionListener(bl);
//		b3.addActionListener(e->ta.append("\r\nGame Over!"));
		ps.add(b3);
		f.add(ps, "South");
		f.pack();
		
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		f.setVisible(true);
		
	}

	private static class BtnListener implements ActionListener {
		
		private TextArea ta;
		
		public BtnListener(TextArea ta){
			this.ta = ta;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			
			ta.append("内部类BtnListener");
			
		}
		
	}
}

