/**
 * Project Name:fundamental
 * File Name:FlowLayoutTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午8:44:31
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:FlowLayoutTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午8:44:31
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:FlowLayoutTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 上午8:44:31 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: FlowLayoutTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月3日 上午8:44:31 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class FlowLayoutTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Frame frame = new Frame("流式布局");
		Button[] btns = new Button[20] ;
//		frame.setLayout(new FlowLayout());
		frame.setBounds(100, 100, 200, 150);
		Panel p = new Panel();
		frame.add(p);
		for(int i=0;i<btns.length;i++){
			btns[i] = new Button();
			btns[i].setName("button"+i);
			btns[i].setLabel("button"+i);
//			frame.add(btns[i]);
			p.add(btns[i]);
		}
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				//frame.dispose();
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}

}

