/**
 * Project Name:fundamental
 * File Name:GridLayoutTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午11:19:09
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:GridLayoutTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午11:19:09
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:GridLayoutTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 上午11:19:09 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: GridLayoutTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月3日 上午11:19:09 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class GridLayoutTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Frame f = new Frame("网格布局测试");
		f.setLayout(new GridLayout(3,4));
		Button[] btns = new Button[13];
		for(int i=0;i<btns.length;i++){
			btns[i] = new Button("button"+i);
			f.add(btns[i]);
		}
		f.setSize(800, 600);
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		
		f.setVisible(true);
	}

}

