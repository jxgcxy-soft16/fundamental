/**
 * Project Name:fundamental
 * File Name:Frame2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月2日下午3:13:01
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Frame2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月2日下午3:13:01
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * ClassName:Frame2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月2日 下午3:13:01 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Frame2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月2日 下午3:13:01 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Frame2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Frame f = new Frame("带面板的窗口");
//		f.setLayout(null);
		
		/*
		Panel p = new Panel();
		p.setBackground(Color.RED);
		p.setSize(400, 200);
		f.add(p,"North");
		p.setLocation(20, 30);*/
		Dimension d = new Dimension(600, 300);
		Panel p2 = new MyPanel(d);
		p2.setBackground(Color.BLUE);
//		p2.setSize(f.getSize());
		f.add(p2/*,"Center"*/);
//		p2.setLocation(40, 30);
		f.setBounds(200, 100, 600, 300);
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		f.pack();
		
		
		f.setVisible(true);
	}

}

class MyPanel extends Panel implements Runnable {
	
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -7001814247339580313L;
	private int x;
	private int y;
	private int bx;
	private int by;
	
	private Random rnd = new Random();
	private final int WIDTH ;//200;
	private final int HEIGHT ;//100;
	
	public MyPanel(Dimension d){
		this.setSize(d);
		WIDTH = this.getWidth();
		HEIGHT = this.getHeight();
		new Thread(this).start();
	}
	public void paint(Graphics g){
		g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 28));
		g.drawString("Good Night!", x, y);
		Color oldColor = g.getColor();
		
		g.setColor(Color.YELLOW);
		g.fillOval(bx, by, 80, 80);
		g.setColor(Color.BLUE);
		g.fillOval(bx-5, bx-5, 60, 60);
		g.setColor(oldColor);
		
		
	}

	@Override
	public void run() {
		while(true){
			try {
				x = rnd.nextInt(WIDTH);
				y = rnd.nextInt(HEIGHT);
				bx = rnd.nextInt(WIDTH);
				by = rnd.nextInt(HEIGHT);
				this.repaint();
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}

