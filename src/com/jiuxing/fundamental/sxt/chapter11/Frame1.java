/**
 * Project Name:fundamental
 * File Name:Frame1.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月2日下午2:08:40
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:Frame1 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月2日 下午2:08:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Frame1 {

	public static void main(String[] args) {
		Frame f = new Frame("测试窗口");
		f.setBounds(200, 100, 400, 300);
		/**
		 * new [接口|抽象类](){
		 * 	 //抽象方法重写
		 * };
		 * 
		 * 接口A, A a = new A(){
		 * 	//接口方法重写
		 * };
		 */
		f.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		f.setLocation(400, 400);
		f.setBackground(Color.GREEN);
		f.setResizable(false);
		f.setVisible(true);
	}

}

