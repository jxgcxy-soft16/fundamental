/**
 * Project Name:fundamental
 * File Name:FlowLayoutTest2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午2:04:00
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:FlowLayoutTest2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 下午2:04:00 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class FlowLayoutTest2 {

	public static void main(String[] args) {
		Frame f = new Frame("流式布局管理器");
//		f.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		/*f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));
		f.add(new Button("BUTTON"));*/
		
		Panel p = new Panel();
		p.setBackground(Color.RED);
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.add(new Button("BUTTON"));
		p.setSize(80, 50);
		f.add(p);
		
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		
		f.setVisible(true);

	}

}

