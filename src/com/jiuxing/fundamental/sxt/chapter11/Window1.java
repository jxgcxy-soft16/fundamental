/**
 * Project Name:fundamental
 * File Name:Window1.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2017年12月29日上午8:48:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.TimeUnit;

/**
 * ClassName:Window1 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月29日 上午8:48:28 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Window1 {

	public static void main(String[] args) {
		Frame f = new Frame("第一个窗口");
		f.setBounds(20, 30, 400, 300);
		
		Canvas canvas = new MyCanvas();
		canvas.setBackground(new Color(23,200,100));
		f.add(canvas);
		f.setVisible(true);
		f.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
	}

}

class MyCanvas extends Canvas {
	
	int x,y;

	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -6934961560781763657L;
	
	public void paint(Graphics g){
		g.setColor(Color.BLUE);
		g.fillOval(30, 40, 200, 150);
		g.setColor(Color.RED);
		g.drawOval(100, 100, 200, 200);
		
		this.moveStr(g);
//		new Thread(()->{moveStr(g);}, "移动文字").start();
		
	}
	
	public void moveStr(Graphics g){
		int x = 0,y=200;
		while(true){
//			g.drawString("欢迎使用awt画布", x, y);
			g.fillRect(x, y, 100, 80);
//			this.repaint();
			x += 10;
			if(x>=this.getWidth()){
				x = 0;
			}
			try {
				TimeUnit.SECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}


