/**
 * Project Name:fundamental
 * File Name:InstantMessager.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11.im
 * Date:2018年1月4日下午4:22:08
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:InstantMessager.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11.im
 * Date:2018年1月4日下午4:22:08
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11.im;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * ClassName:InstantMessager <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月4日 下午4:22:08 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: InstantMessager <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月4日 下午4:22:08 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class InstantMessager extends JFrame {

	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -2041390078676281070L;

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		if(null!=args&&args.length>0){
			String nickname = args[0];
			InstantMessager im = new InstantMessager(nickname);
			im.setVisible(true);
		} else {
			System.out.println("未注册昵称！");
		}
		
	}
	
	InstantMessager(String nickname){
		super(nickname);
		this.nickname = nickname;
		this.getContentPane().add(headPanel, BorderLayout.NORTH);
		this.getContentPane().add(ta);
		this.getContentPane().add(footPanel, BorderLayout.SOUTH);
		this.setSize(524, 499);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public JPanel creatHeadPanel(){
		JPanel p = new JPanel();
		URL url = this.getClass().getResource("/assets/image/bluecat.jpg");
		ImageIcon icon = new ImageIcon(url);
		icon.setImage(icon.getImage().getScaledInstance(50,  
                50, Image.SCALE_DEFAULT));  
		JLabel label = new JLabel("即时聊天demo",icon, JLabel.CENTER);
		p.setLayout(new FlowLayout(FlowLayout.LEFT));
		p.add(label);
		return p;
	}
	
	public JPanel createFootPanel(){
		JPanel p = new JPanel();
		JButton btn = new JButton("发送");
		
		btn.addActionListener(e->sendMsg());
		tf.addKeyListener(keyListener);
		p.setLayout(new BorderLayout());
		p.add(tf);
		p.add(btn, BorderLayout.EAST);
		return p;
	}
	
	public void sendMsg(){
		String msg = tf.getText();
		StringBuilder sb = new StringBuilder(msg);
		sb.insert(0, ":").insert(0, this.nickname);
		String m = sb.toString();
		udpClient.send(m);
		ta.append(m);
		ta.append("\r\n");
		tf.setText("");
	}
	
	private String nickname = "某某";
	private JPanel headPanel = creatHeadPanel();
	
	private JTextArea ta = new JTextArea(20,30);
	
	private JTextField tf = new JTextField(20);
	
	private KeyListener keyListener = new KeyAdapter(){
		@Override
		public void keyReleased(KeyEvent e) {
			if(e.getKeyCode()==KeyEvent.VK_ENTER){
				sendMsg();
			}
		}
	};
	
	private JPanel footPanel = createFootPanel();
	
	private UdpClient udpClient = new UdpClient();
	
	private class UdpClient implements Runnable {
		
		private byte[] buf = new byte[1024];
		private DatagramPacket dp = new DatagramPacket(buf, buf.length);
		private DatagramSocket ds;
		private InetSocketAddress isa;
		private InetSocketAddress myAddr;
		
		UdpClient(){
			this.config();
			this.startServer();
			new Thread(this).start();
		}
		
		public void startServer(){
			try {
				ds = new DatagramSocket(myAddr);
			} catch (SocketException e) {
				e.printStackTrace();
			}
		}
		
		public void config(){
			if(null==isa){
				String destHost = System.getProperty("dest.host", "localhost");
				String destPort = System.getProperty("dest.port", "7001");
				isa = new InetSocketAddress(destHost,Integer.parseInt(destPort));
			}
			if(null==myAddr){
				String srcHost = System.getProperty("src.host", "localhost");
				String srcPort = System.getProperty("src.port", "7000");
				myAddr = new InetSocketAddress(srcHost,Integer.parseInt(srcPort));
			}
		}
		
		public void send(String msg){
			try {
				byte[] bufout = msg.getBytes();
				DatagramPacket dpo = new DatagramPacket(bufout, bufout.length, isa);
				ds.send(dpo);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run(){
			while(true){
				
				try {
					ds.receive(dp);
					String rcvMsg = new String(buf,0,dp.getLength());
					ta.append(rcvMsg);
					ta.append("\r\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

