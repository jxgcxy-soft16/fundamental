/**
 * Project Name:fundamental
 * File Name:BorderLayoutTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午9:04:38
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:BorderLayoutTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午9:04:38
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * ClassName:BorderLayoutTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 上午9:04:38 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: BorderLayoutTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月3日 上午9:04:38 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class BorderLayoutTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		Frame f = new Frame("边框布局管理器");
		
//		f.setLayout(null);
		f.add(new Button("北丐"), BorderLayout.NORTH);
		f.add(new Button("东邪"), BorderLayout.EAST);
		f.add(new Button("南帝"), BorderLayout.SOUTH);
		f.add(new Button("西毒"), BorderLayout.WEST);
		Button btnCenter = new Button("中神通");
		btnCenter.setSize(800, 600);
		btnCenter.setLocation(500, 400);
		f.add(btnCenter, BorderLayout.CENTER);
		btnCenter.addMouseListener(new MouseAdapter(){
			private String oldStr;
			public void mouseEntered(MouseEvent e) {
				oldStr = btnCenter.getLabel();
				btnCenter.setLabel("论剑开始");
			}
			
			public void mouseExited(MouseEvent e) {
				
				btnCenter.setLabel("论剑结束");
			}
			
			public void mouseClicked(MouseEvent e) {
				btnCenter.setLabel(oldStr);
			}
		});
		
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		f.setSize(400, 300);
//		f.pack();
		f.setVisible(true);
		
	}

}

