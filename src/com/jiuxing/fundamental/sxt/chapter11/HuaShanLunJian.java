/**
 * Project Name:fundamental
 * File Name:HuaShanLunJian.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午2:16:58
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:HuaShanLunJian.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日下午2:16:58
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;

/**
 * ClassName:HuaShanLunJian <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 下午2:16:58 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: HuaShanLunJian <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月3日 下午2:16:58 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class HuaShanLunJian {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		Frame f = new Frame("华山论剑");
		
		f.add(new Button("北丐"), BorderLayout.NORTH);
		f.add(new Button("东邪"), BorderLayout.EAST);
		f.add(new Button("南帝"), BorderLayout.SOUTH);
		f.add(new Button("西毒"), BorderLayout.WEST);
		f.add(new Button("中神通"), BorderLayout.CENTER);
		
		f.add(new ImgBtn()/*Button("周伯通")*/, BorderLayout.CENTER);
		f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		Image image = Toolkit.getDefaultToolkit().getImage("C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg");
		f.setIconImage(image);
		f.pack();
		f.setVisible(true);

	}

}

class ImgBtn extends Button {

	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = 8452267543438218886L;
	
	public void paint(Graphics g) {
//		String oldPath = "C:\\Users\\Public\\Pictures\\Sample Pictures\\Penguins.jpg";
//		Image img = Toolkit.getDefaultToolkit().getImage(oldPath);
		URL url = this.getClass().getResource("/assets/image/th.jpg");
		Image img2 = Toolkit.getDefaultToolkit().getImage(url);
		g.drawImage(img2, 0, 0, null);
    }
}

