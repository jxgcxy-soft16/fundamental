/**
 * Project Name:fundamental
 * File Name:TenButtons.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter11
 * Date:2018年1月3日上午11:44:36
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter11;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;

/**
 * ClassName:TenButtons <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月3日 上午11:44:36 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class TenButtons {

	public static void main(String[] args) {
		Frame f = new Frame("十常侍");
		
		GridLayout gl = new GridLayout(2,1);
		Panel pe = new Panel();
		pe.setLayout(gl);
		pe.add(new Button("BUTTON"));
		pe.add(new Button("BUTTON"));
		f.add(pe, BorderLayout.EAST);
		
		Panel pw = new Panel();
		pw.setLayout(gl);
		pw.add(new Button("BUTTON"));
		pw.add(new Button("BUTTON"));
		f.add(pw, BorderLayout.WEST);
		
		Panel pc = new Panel();
		pc.setLayout(gl);
		Panel pcu = new Panel();
		pcu.setLayout(gl);
		pcu.add(new Button("BUTTON"));
		pcu.add(new Button("BUTTON"));
		
		pc.add(pcu);
		
		Panel pcd = new Panel();
		pcd.setLayout(new GridLayout(2,2));
		pcd.add(new Button("BUTTON"));
		pcd.add(new Button("BUTTON"));
		pcd.add(new Button("BUTTON"));
		pcd.add(new Button("BUTTON"));
		
		pc.add(pcd);
		
		f.add(pc, BorderLayout.CENTER);
		
		f.pack();
		f.setVisible(true);

	}

}

