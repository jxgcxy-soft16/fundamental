/**
 * Project Name:fundamental
 * File Name:NullPointExceptionTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter04
 * Date:2018年1月10日上午8:46:03
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter04;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * ClassName:NullPointExceptionTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月10日 上午8:46:03 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class NullPointExceptionTest {

	public static void main(String[] args) {
		handleEx();
	}
	
	static void handleEx() {
		try {
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(""));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(Exception ex){
			ex.printStackTrace();
		} 
	}

}

