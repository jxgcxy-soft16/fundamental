/**
 * Project Name:fundamental
 * File Name:ExceptionTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter04
 * Date:2017年11月22日上午8:43:27
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter04;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;

/**
 * ClassName:ExceptionTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月22日 上午8:43:27 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ExceptionTest {

	public static void main(String[] args) throws IOException {
//		int a = 10/0;
//		System.out.println("a="+a);
		
//		int [] b = new int[]{1,2,3};
//		System.out.println("b="+b[0]);
//		try {
//			print(null);
//		} 
//		catch(MissingArgsException mae){
//			System.out.println(mae.getMessage());
//			mae.printStackTrace();
//		}
//		System.out.println("hello");
		Date d1 = new Date();
		java.sql.Date d2 = new java.sql.Date(22);
		String a = "hello";
		String b = "hello";
		System.out.println("a==b:"+(a==b));
		int[] arr = null;
		System.out.println("arr is Object:"+(arr instanceof Object));
		A b1 = new B1();
//		A b2 = new B2();
		A b3 = new B3();
//		A b4 = new B4();
		A b5 = new B5();
		
		b1.a();
//		b2.a();
		b3.a();
//		b4.a();
		b5.a();
		
	}
	
	public static void print(PrintStream out) throws MissingArgsException{
		if(null==out){
			throw new MissingArgsException("参数为空aasdsssfs");
		}
		out.println("xxsdfdsfsdf");
	}

}

class MissingArgsException extends Exception {

	public MissingArgsException() {
		
		super();
		// TODO Auto-generated constructor stub
		
	}

	public MissingArgsException(String message) {
		
		super(message);
		// TODO Auto-generated constructor stub
		
	}
	
}

/*abstract*/ class A {
	public /*abstract*/ void a() throws IOException {
		System.out.println("A");
	};
}

class B1 extends A {
	public void a() throws FileNotFoundException {
		System.out.println("B1");
	}
}

/*class B2 extends A {
	public void a() throws Exception {
		System.out.println("B2");
	}
}*/

class B3 extends A {
	public void a()  {
		System.out.println("B3");
	}
}


/*class B4 extends A {
	public void a() throws IOException,MissingArgsException {
		System.out.println("B4");
	}
}*/

class B5 extends A {
	public void a() throws IOException {
		System.out.println("B5");
	}
}




