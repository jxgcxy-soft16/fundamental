/**
 * Project Name:fundamental
 * File Name:IOExceptionTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter04
 * Date:2017年11月22日下午2:57:59
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:IOExceptionTest.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter04
 * Date:2017年11月22日下午2:57:59
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter04;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * ClassName:IOExceptionTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月22日 下午2:57:59 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: IOExceptionTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月22日 下午2:57:59 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class IOExceptionTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		File file = new File("D:\\download\\education\\demo\\mytest.txt");
		Reader in = null;
		try {
			in = new FileReader(file);
			char[] cbuf = new char[1024];
			int count = 0;
			while((count = in.read(cbuf))!=-1){
				String text = new String(cbuf);
				System.out.println("text="+text);
			}
		} 
		catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}
		catch(  IOException ie){
			ie.printStackTrace();
		}
		
		
		
		
		finally {
			if(null!=in){//关闭文件
				try {
					in.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}

