/**
 * Project Name:fundamental
 * File Name:TcpSocketServer2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日下午2:54:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TcpSocketServer2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日下午2:54:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * ClassName:TcpSocketServer2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月25日 下午2:54:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TcpSocketServer2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月25日 下午2:54:40 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TcpSocketServer3 {

	static final String PREFIX_SERVER = "server:";
	static final String PREFIX_CLIENT = "client:";
	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		
	}
	
	public void start(){
		ServerSocket ss = null;
		Socket s = null;
		try {
			System.out.println("starting tcp server ...");
			
			ss = new ServerSocket(6000);
			s = ss.accept();//accept阻塞式方法
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			if(s!=null){
				try {
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null!=ss){
				try {
					ss.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
	
	public void writeMsg(Socket s){
		DataOutputStream dos = null;
		Scanner scanner = null;
		try {
			dos = new DataOutputStream(s.getOutputStream());
			String tip = " 客户端已经连接上来,it's from "+s.getInetAddress();
			System.out.println(PREFIX_SERVER+tip);
			dos.writeUTF(PREFIX_SERVER+tip);
			
			scanner = new Scanner(System.in);
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				System.out.println(PREFIX_SERVER+line);
				dos.writeUTF(PREFIX_SERVER+line);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			if(dos!=null){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(scanner!=null){
				scanner.close();
			}
		}
	}
	
	public void readMsg(Socket s){
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(s.getInputStream());
			String clientMsg = dis.readUTF();
			System.out.println(PREFIX_CLIENT+clientMsg);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			if(dis!=null){
				try {
					dis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

