/**
 * Project Name:fundamental
 * File Name:ChtRmSvr.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2018年1月9日下午9:40:21
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ChtRmSvr.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2018年1月9日下午9:40:21
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ClassName:ChtRmSvr <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月9日 下午9:40:21 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ChtRmSvr <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月9日 下午9:40:21 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ChtRmSvr {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		ChtRmSvr chtRmSvr = new ChtRmSvr();
		chtRmSvr.start();
		chtRmSvr.listen();
	}
	
	public void start(){
		try {
			ss = new ServerSocket(8000);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void listen(){
		try {
			while(true){
				Socket s = ss.accept();
				ClientConn clientConn = new ClientConn(s);
				this.clientConnList.add(clientConn);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private ServerSocket ss = null;
	private List<ClientConn> clientConnList = new ArrayList<>();

	class ClientConn implements Runnable {
		private Socket s;
		
		ClientConn(Socket s){
			this.s = s;
			new Thread(this).start();
		}
		
		public void send(String msg) throws IOException {
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			dos.writeUTF(msg);
		}
		
		@Override
		public void run() {
			DataInputStream dis = null;
			try {
				dis = new DataInputStream(s.getInputStream());
				while(true){
					String msg = dis.readUTF();
					for(Iterator<ClientConn> iter = clientConnList.iterator();iter.hasNext();){
						ClientConn clientConn = iter.next();
						if(clientConn!=this){
							try {
								clientConn.send(msg);
							}
							catch (IOException e) {
								e.printStackTrace();
							}
						}
						
					}
				}
				
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				if(dis!=null){
					try {
						dis.close();
					} 
					catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(s!=null){
					try {
						s.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				clientConnList.remove(this);
			}
		}
		
	}
}

