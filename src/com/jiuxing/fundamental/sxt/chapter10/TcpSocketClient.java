/**
 * Project Name:fundamental
 * File Name:TcpSocketClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日上午9:10:16
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TcpSocketClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日上午9:10:16
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * ClassName:TcpSocketClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月25日 上午9:10:16 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TcpSocketClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月25日 上午9:10:16 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TcpSocketClient {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		try {
			Socket s = new Socket();
			s.connect(new InetSocketAddress("localhost", 9000));
			DataInputStream dis = new DataInputStream(s.getInputStream());
			String resp = dis.readUTF();
			System.out.println("服务端:"+resp);
			
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			dos.writeUTF("Merry Christmas !");
			dis.close();
			dos.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

