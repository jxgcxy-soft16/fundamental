/**
 * Project Name:fundamental
 * File Name:UdpMessageServer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午4:46:30
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UdpMessageServer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午4:46:30
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * ClassName:UdpMessageServer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月26日 下午4:46:30 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UdpMessageServer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月26日 下午4:46:30 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UdpMessageServer {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @since JDK 1.6
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		byte[] buf = new byte[1024];
		DatagramPacket dp = new DatagramPacket(buf, buf.length);
		DatagramSocket ds = new DatagramSocket(6000);
		ds.receive(dp);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(buf,0,dp.getLength());
		ObjectInputStream ois = new ObjectInputStream(bais);
		Message msg = (Message)ois.readObject();
		System.out.println("msg rcv:"+msg);
		ds.close();
	}

}

