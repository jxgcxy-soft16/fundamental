/**
 * Project Name:fundamental
 * File Name:UdpClt.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午2:25:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UdpClt.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午2:25:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Scanner;

/**
 * ClassName:UdpClt <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月26日 下午2:25:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UdpClt <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月26日 下午2:25:40 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UdpClt {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @throws IOException 
	 * @since JDK 1.6
	 */
	public static void main(String[] args) throws IOException {
		byte[] buf = new byte[1024];//"新的一年快到了".getBytes();
		DatagramPacket dp = 
		  new DatagramPacket(buf,buf.length
				,new InetSocketAddress("localhost",5000));
		final DatagramSocket ds = new DatagramSocket(5001);
		
		Runnable readThread = ()->{
			byte[] inbuf = new byte[1024];
			DatagramPacket indp = 
					  new DatagramPacket(inbuf,inbuf.length);
			while(true){
				try {
					ds.receive(indp);
				} catch (Exception e) {
					e.printStackTrace();
				}
				String inStr = new String(inbuf,0,indp.getLength());
				if("\r\n".equals(inStr)||"\n".equals(inStr)||"".equals(inStr)){
					continue;
				}
				System.out.println("\nUdpSrv:"+inStr);
			}
			
		};
		new Thread(readThread,"读取器").start();
		
		System.out.print("UdpClt:");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		while(!"byte".equals(line=br.readLine())){
			if("\r\n".equals(line)||"\n".equals(line)||"".equals(line)){
				System.out.print("UdpClt:");
				continue;
			}
			byte[] b = line.getBytes();
			dp.setData(b);
			ds.send(dp);
		}
		br.close();
		ds.close();
	}

}

