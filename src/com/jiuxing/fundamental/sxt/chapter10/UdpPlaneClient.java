/**
 * Project Name:fundamental
 * File Name:UdpPlaneClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月27日下午2:24:41
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * ClassName:UdpPlaneClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月27日 下午2:24:41 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class UdpPlaneClient {

	public static void main(String[] args) throws IOException {
		Plane enemy = new Plane(200.34,300.38,500,200,1000,"敌军战机");
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(enemy);
		
		byte[] buf = baos.toByteArray();
		DatagramPacket dp = new DatagramPacket(buf, buf.length, new InetSocketAddress("localhost", 6000));
		DatagramSocket ds = new DatagramSocket(6001);
		ds.send(dp);
		oos.close();
		ds.close();
		
	}

}

