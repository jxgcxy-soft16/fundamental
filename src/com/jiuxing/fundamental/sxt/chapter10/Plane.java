/**
 * Project Name:fundamental
 * File Name:Plane.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月27日下午2:13:50
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Plane.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月27日下午2:13:50
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;
/**
 * ClassName:Plane <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月27日 下午2:13:50 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Plane <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月27日 下午2:13:50 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Plane implements java.io.Serializable {
	
	
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -6659699556241864949L;
	
	private double x;
	private double y;
	private int length;
	private int width;
	private int speed;
	private String name;
	
	
	
	public Plane(double x, double y, int length, int width, int speed, String name) {
		super();
		this.x = x;
		this.y = y;
		this.length = length;
		this.width = width;
		this.speed = speed;
		this.name = name;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("{name:").append(name)
		.append(",x:").append(x)
		.append(",y:").append(y)
		.append(",length:").append(length)
		.append(",width:").append(this.width)
		.append(",speed:").append(this.speed)
		.append("}");
		return sb.toString();
	}
	
}

