/**
 * Project Name:fundamental
 * File Name:UdpSocketClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月23日下午11:36:37
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UdpSocketClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月23日下午11:36:37
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * ClassName:UdpSocketClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月23日 下午11:36:37 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UdpSocketClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月23日 下午11:36:37 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UdpSocketClient {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @throws IOException 
	 * @since JDK 1.6
	 */
	public static void main(String[] args) throws IOException {
		byte[] buf = "Hello udp".getBytes();
		DatagramPacket dp = new DatagramPacket(buf, buf.length, new InetSocketAddress("localhost", 7000) );
		DatagramSocket ds = new DatagramSocket(8000);
		ds.send(dp);
		ds.close();
	}

}

