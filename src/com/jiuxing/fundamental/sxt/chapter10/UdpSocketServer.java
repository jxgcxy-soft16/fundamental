/**
 * Project Name:fundamental
 * File Name:UdpSocketServer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月23日下午11:03:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * ClassName:UdpSocketServer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月23日 下午11:03:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class UdpSocketServer {

	public static void main(String[] args) throws IOException {
		byte[] buf = new byte[1024];
		DatagramPacket dp = new DatagramPacket(buf, buf.length);
		DatagramSocket ds = new DatagramSocket(7000);
		ds.receive(dp);
		System.out.println("received msg:"+new String(buf, 0, dp.getLength()));
		ds.close();
	}

}

