/**
 * Project Name:fundamental
 * File Name:TcpSocketClient2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日下午3:12:23
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TcpSocketClient2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日下午3:12:23
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * ClassName:TcpSocketClient2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月25日 下午3:12:23 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TcpSocketClient2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月25日 下午3:12:23 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TcpSocketClient2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Socket s = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		try {
			s = new Socket();
			s.connect(new InetSocketAddress("localhost",6000));
			dis = new DataInputStream(s.getInputStream());
			String serverMsg = dis.readUTF();
			System.out.println("server:"+serverMsg);
			
			dos = new DataOutputStream(s.getOutputStream());
			dos.writeUTF("服务器，你好!");
		} 
		catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
			if(dis!=null){
				try {
					dis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(dos!=null){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null!=s){
				try {
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}

