/**
 * Project Name:fundamental
 * File Name:TcpSocketServer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日上午9:02:18
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TcpSocketServer.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日上午9:02:18
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * ClassName:TcpSocketServer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月25日 上午9:02:18 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TcpSocketServer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月25日 上午9:02:18 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TcpSocketServer {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		ServerSocket ss;
		try {
			ss = new ServerSocket(9000);
			Socket s = ss.accept();
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			DataInputStream dis = new DataInputStream(s.getInputStream());
			dos.writeUTF("欢迎光临..."+s.getInetAddress());
			String clientMsg = dis.readUTF();
			System.out.println("客户端:"+clientMsg);
			dos.close();
			dis.close();
			s.close();
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}

