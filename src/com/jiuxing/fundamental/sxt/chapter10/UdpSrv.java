/**
 * Project Name:fundamental
 * File Name:UdpSrv.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午2:19:27
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UdpSrv.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午2:19:27
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Scanner;

/**
 * ClassName:UdpSrv <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月26日 下午2:19:27 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UdpSrv <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月26日 下午2:19:27 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UdpSrv {

	DatagramSocket ds = null;
	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @throws IOException 
	 * @since JDK 1.6
	 */
	public static void main(String[] args)  {
		UdpSrv udpSrv = new UdpSrv();
		udpSrv.start();
	}
	
	public void start()  {
		try {
			 ds = new DatagramSocket(5000);
			final UdpSrv _this = this;
			new Thread(()->{_this.receive(ds);},"recv").start();
			this.send(ds);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
			if(null!=ds){
				ds.close();
			}
		}
	}
	
	public void receive(DatagramSocket ds) {
		byte[] buf = new byte[1024];
		DatagramPacket dp = new DatagramPacket(buf,buf.length);
		System.out.println("服务器在5000端口监听");
		
		while(!ds.isClosed()){
			try {
				ds.receive(dp);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String msgRcv = new String(buf, 0, dp.getLength());
			if("\r\n".equals(msgRcv)||"\n".equals(msgRcv)||"".equals(msgRcv)){
				continue;
			}
			System.out.println("\nUdpClt:"+msgRcv);
			if("byte".equalsIgnoreCase(msgRcv)){
				break;
			}
		}
	}
	
	public void send(DatagramSocket ds) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("UdpSrv:");
		String line = null;
        while(!"byte".equals(line = br.readLine())){
        	if("\r\n".equals(line)||"\n".equals(line)||"".equals(line)){
				System.out.print("UdpSrv:");
				continue;
			}
			byte[] b = line.getBytes();
			DatagramPacket dp = new DatagramPacket(b,b.length,new InetSocketAddress("localhost",5001));
			ds.send(dp);
		}
        br.close();
	}

}

