/**
 * Project Name:fundamental
 * File Name:TcpSocketServer2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日下午2:54:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:TcpSocketServer2.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月25日下午2:54:40
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * ClassName:TcpSocketServer2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月25日 下午2:54:40 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: TcpSocketServer2 <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月25日 下午2:54:40 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class TcpSocketServer2 {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		ServerSocket ss = null;
		Socket s = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		try {
			System.out.println("starting tcp server ...");
			ss = new ServerSocket(6000);
			s = ss.accept();//accept阻塞式方法
			
			System.out.println("There is a client which connected  server");
			
			dos = new DataOutputStream(s.getOutputStream());
			dos.writeUTF(" 客户端已经连接上来,it's from "+s.getInetAddress());
			
			dis = new DataInputStream(s.getInputStream());
			String clientMsg = dis.readUTF();
			System.out.println("client:"+clientMsg);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			if(dos!=null){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(dis!=null){
				try {
					dis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(s!=null){
				try {
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null!=ss){
				try {
					ss.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
	}

}

