/**
 * Project Name:fundamental
 * File Name:UdpMessageClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午4:50:25
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UdpMessageClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午4:50:25
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Date;

/**
 * ClassName:UdpMessageClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月26日 下午4:50:25 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UdpMessageClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月26日 下午4:50:25 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UdpMessageClient {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @throws IOException 
	 * @since JDK 1.6
	 */
	public static void main(String[] args) throws IOException {
		
		Message msg = new Message();
		msg.setTitle("晚会");
		msg.setContent("全体同学晚上7点开始晚会");
		msg.setTime(new Date());
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(msg);
		
		byte[] buf = baos.toByteArray();
		DatagramPacket dp = new DatagramPacket(buf,buf.length,new InetSocketAddress("localhost", 6000));
		DatagramSocket ds = new DatagramSocket(6001);
		ds.send(dp);
		ds.close();
	}
	

}

