/**
 * Project Name:fundamental
 * File Name:UdpClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日上午10:42:24
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UdpClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日上午10:42:24
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * ClassName:UdpClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月26日 上午10:42:24 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UdpClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月26日 上午10:42:24 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UdpClient {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @throws IOException 
	 * @since JDK 1.6
	 */
	public static void main(String[] args) throws IOException {
		byte[] buf = "hello, udp 服务器".getBytes();
		DatagramPacket dp = new DatagramPacket(buf,buf.length,new InetSocketAddress("localhost",3000));
		DatagramSocket ds = new DatagramSocket();
		ds.send(dp);
		ds.close();
	}

}

