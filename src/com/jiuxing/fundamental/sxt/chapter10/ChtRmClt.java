/**
 * Project Name:fundamental
 * File Name:ChtRmClt.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2018年1月9日下午10:04:44
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ChtRmClt.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2018年1月9日下午10:04:44
 * Copyright (c) 2018, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * ClassName:ChtRmClt <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2018年1月9日 下午10:04:44 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ChtRmClt <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2018年1月9日 下午10:04:44 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ChtRmClt {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		ChtRmClt chtRmClt = new ChtRmClt();
		chtRmClt.start();
	}
	
	private void start() {
		
		try {
			String host = System.getProperty("chat.host", "localhost");
			s = new Socket(host, 8000);
			new Receiver();
			send();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void send(){
		try(Scanner scanner = new Scanner(System.in)){
			try {
				DataOutputStream dos = new DataOutputStream(s.getOutputStream());
				while(scanner.hasNextLine()){
					String line = scanner.nextLine();
					dos.writeUTF(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	class Receiver implements Runnable {
		Receiver(){
			new Thread(this).start();
		}
		@Override
		public void run() {
			try {
				DataInputStream dis = new DataInputStream(s.getInputStream());
				while(true){
					String msg = dis.readUTF();
					System.out.println(msg);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(s!=null&&s.isClosed()){
					try {
						s.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}

	private Socket s = null;

}

