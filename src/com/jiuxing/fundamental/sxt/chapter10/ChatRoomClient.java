/**
 * Project Name:fundamental
 * File Name:ChatRoomClient.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月28日下午9:58:53
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * ClassName:ChatRoomClient <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月28日 下午9:58:53 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ChatRoomClient {

	public static void main(String[] args) {
		if(null!=args&&args.length>0){
			String nickname = args[0];
			ChatRoomClient crc = new ChatRoomClient(nickname);
			crc.login();
		} else {
			System.out.println("请按如下格式启动程序:");
			System.out.println("java com.jiuxing.fundamental.sxt.chapter10.ChatRoomClient nickname");
		}
		
	}
	
	public ChatRoomClient(String nickname){
		this.nicknanme = nickname;
	}
	
	public void login(){
		Socket s = new Socket();
		try {
			s.connect(new InetSocketAddress("localhost",ChatRoomServer.PORT));
			new Thread(()->{this.receiveMsg(s);}).start();
			sendMsg(s);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(null!=s){
				try {
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void sendLogin(DataOutputStream dos) throws IOException{
			dos.writeUTF(this.nicknanme+"进入聊天室");
//			bw.write(this.nicknanme+"进入聊天室");
//			bw.newLine();
//			bw.flush();
			
	}
	
	public void sendMsg(final Socket s){
		Scanner scanner = null;
		DataOutputStream dos = null;
		BufferedWriter bw = null;
		try {
			dos = new DataOutputStream(s.getOutputStream());
//			bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			this.sendLogin(dos);
			scanner = new Scanner(System.in);
			while(scanner.hasNextLine()){
				
				System.out.println("client socket is closed?"+s.isClosed());
				if(s.isClosed()){
					s.connect(SERVER_ADDRESS);
				}
				String line = scanner.nextLine();
				dos.writeUTF(line);
				dos.flush();
//				bw.write(line);
//				bw.newLine();
//				bw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			scanner.close();
			if(null!=dos){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			/*if(null!=bw){
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}*/
		}
	}
	
	public void receiveMsg(final Socket s){
		DataInputStream dis = null;
		BufferedReader br = null;
		try {
			dis = new DataInputStream(s.getInputStream());
			String msg = dis.readUTF();
//			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
//			String msg = br.readLine();
			System.out.println("服务器:"+msg);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null!=dis){
				try {
					dis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
//			if(null!=br){
//				try {
//					br.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
		}
	}

	private static final InetSocketAddress SERVER_ADDRESS = new InetSocketAddress("localhost",ChatRoomServer.PORT);
	private String nicknanme = "anynamous";
}

