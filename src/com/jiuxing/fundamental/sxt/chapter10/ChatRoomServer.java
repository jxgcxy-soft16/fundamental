/**
 * Project Name:fundamental
 * File Name:ChatRoom.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月27日下午4:58:07
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * ClassName:ChatRoom <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月27日 下午4:58:07 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ChatRoomServer {

	public static void main(String[] args) {

		ChatRoomServer chatRoomServer = new ChatRoomServer();
		chatRoomServer.start();
	}
	
	public void start(){
		try {
			ss = new ServerSocket(PORT);
			new Thread(this::prompt).start();
			Socket s = null;
			
			while(this.acpt&&(s = ss.accept())!=null){
				this.addSocket(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void prompt(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("聊天室服务器已启动,退出请输入[quit]");
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			if("quit".equals(line)){
				this.acpt = false;
				this.stop();
			}
		}
		scanner.close();
	}
	
	public void stop(){
		try {
			this.broadcast("聊天服务器已退出");
			socketList.forEach(this::disconnect);
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void disconnect(Socket s){
		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addSocket(final Socket s){
		socketList.add(s);
		final ChatRoomServer _this = this;
		Runnable r = ()->{
			try {
//				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				DataInputStream dis = new DataInputStream(s.getInputStream());
				while(true){
					String msgRcv = null;
					while((msgRcv = dis.readUTF())!=null){
						_this.broadcast(msgRcv);
					}
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
//				System.out.println("addSocket thread end ...");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
		new Thread(r).start();
//		executor.execute(r);
	}
	
	public void broadcast(String msg, Socket s) throws IOException{
		for(Iterator<Socket> iter = socketList.iterator();iter.hasNext();){
			Socket otherSocket = iter.next();
			if(!otherSocket.equals(s)){
				DataOutputStream dos = new DataOutputStream(otherSocket.getOutputStream());
				/*BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(otherSocket.getOutputStream()));
				bw.write(msg);
				bw.newLine();
				bw.flush();*/
				dos.writeUTF(msg);
			}
		}
	}
	
	public void broadcast(String msg) throws IOException{
			for(Iterator<Socket> iter = socketList.iterator();iter.hasNext();){
				Socket otherSocket = iter.next();
				if(otherSocket.isClosed()){
					System.err.println("connection closed");
					iter.remove();
				} else {
//					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(otherSocket.getOutputStream()));
//					bw.write(msg);
//					bw.newLine();
//					bw.flush();
					DataOutputStream dos = new DataOutputStream(otherSocket.getOutputStream());
					dos.writeUTF(msg);
				}
//				DataOutputStream dos = new DataOutputStream(otherSocket.getOutputStream());
//				dos.writeUTF(msg);
			}
		
	}

	public static final int PORT = 8000;
	
	private volatile boolean acpt = true;
	private final Executor executor = Executors.newCachedThreadPool();
	private ServerSocket ss = null;
	private final List<Socket> socketList = new ArrayList<Socket>();
}

