/**
 * Project Name:fundamental
 * File Name:Message.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午4:43:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Message.java
 * Package Name:com.jiuxing.fundamental.sxt.chapter10
 * Date:2017年12月26日下午4:43:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.sxt.chapter10;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName:Message <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年12月26日 下午4:43:43 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Message <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年12月26日 下午4:43:43 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Message implements Serializable {

	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = 4336810790383078491L;

	private String title;
	private String content;
	private Date   time;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	@Override
	public String toString(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:MM:ss");
		String t = sdf.format(time);
		StringBuilder sb = new StringBuilder();
		sb.append("{title:").append(this.title)
		.append(",content:").append(this.content)
		.append(",time:").append(t)
		.append("}");
		return sb.toString();
	}
}

