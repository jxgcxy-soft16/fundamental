/**
 * Project Name:fundamental
 * File Name:Register.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月7日上午10:51:21
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Register.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月7日上午10:51:21
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter15;

import java.util.Scanner;

/**
 * ClassName:Register <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月7日 上午10:51:21 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Register <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月7日 上午10:51:21 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Register {

	/**
	 * 
	 * verify:验证用户名、密码长度及两次输入的密码是否相同. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param name
	 * @param pwd
	 * @param repwd
	 * @return
	 * @since JDK 1.6
	 */
	public boolean verify(String name, String pwd, String repwd){
		if(name.length()<3||pwd.length()<6){//用户名长度小于3或密码长度小于6
			System.out.println("用户名长度不小于3，密码长度不小于6!");
			return false;
		}
		if(!pwd.equals(repwd)){//两次密码输入不相等
			System.out.println("两次输入的密码不相同!");
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * inputRegInfo:从控制台输入注册信息. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void inputRegInfo(){
		Scanner scanner = new Scanner(System.in);
		boolean checked = false;
		do {
			System.out.print("请输入用户名:");
			String name = scanner.next();
			System.out.print("请输入密码:");
			String pwd = scanner.next();
			System.out.print("请再次输入密码:");
			String repwd = scanner.next();
			checked = verify(name, pwd, repwd);
			if(checked){
				System.out.println("注册成功,请牢记用户名和密码.");
			}
		} while(!checked);
		
		scanner.close();
	}
	
	public static void main(String[] args){
		Register register = new Register();
//		register.inputRegInfo();
		register.inputRegInfo2();
	}
	
	/**
	 * 
	 * checkRegInfo:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param id 身份证号
	 * @param mobile 手机号
	 * @param tel 座机号
	 * @return
	 * @since JDK 1.6
	 */
	public boolean checkRegInfo(String id, String mobile, String tel){
		
		/**
		 * 验证身份证号
		 */
		int len = id.length();
		if(len!=16&&len!=18){
			System.out.println("身份证号必须是16或18位!");
			return false;
		}
		
		/**
		 * 验证手机号
		 */
		len = mobile.length();
		if(len!=11){
			System.out.println("手机号必须是11位!");
			return false;
		}
		
		/**
		 * 验证座机号
		 */
		String[] tels = tel.split("-");
		if(tels.length!=2){
			System.out.println("座机号输入格式不对!");
			return false;
		} else if(tels[0].length()!=4||tels[1].length()!=7){
			System.out.println("座机号码必须是4位,电话号码必须是7位!");
			return false;
		}
		
		System.out.println("注册成功!");
		return true;
	}
	
	/**
	 * 
	 * inputRegInfo2:输入会员注册信息. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void inputRegInfo2(){
		Scanner scanner = new Scanner(System.in);
		boolean checked = false;
		do {
			System.out.println("***欢迎进入注册系统***");
			System.out.print("请输入身份证:");
			String id = scanner.next();
			System.out.print("请输入手机号:");
			String mob = scanner.next();
			System.out.print("请输入座机号:");
			String t = scanner.next();
			
			checked = this.checkRegInfo(id, mob, t);
		} while(!checked);
		
		scanner.close();
		
	}
}

