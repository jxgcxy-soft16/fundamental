/**
 * Project Name:fundamental
 * File Name:Goods.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月9日下午3:32:29
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Goods.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月9日下午3:32:29
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter15;
/**
 * ClassName:Goods <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月9日 下午3:32:29 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Goods <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月9日 下午3:32:29 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Goods {

	/**
	 * 商品编号
	 */
	int gdno;
	
	/**
	 * 商品名称
	 */
	String gdname;
	
	/**
	 * 商品价格
	 */
	double gdprice;
	
	/**
	 * 
	 * Creates a new instance of Goods.
	 *
	 * @param gdno 商品编号
	 * @param gdname 商品名称
	 * @param gdprice 商品价格
	 */
	public Goods(int gdno, String gdname, double gdprice){
		this.gdno = gdno;
		this.gdname = gdname;
		this.gdprice = gdprice;
	}
}

