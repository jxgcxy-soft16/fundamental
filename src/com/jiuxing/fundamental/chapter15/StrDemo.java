/**
 * Project Name:fundamental
 * File Name:StrDemo.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月7日上午11:35:02
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:StrDemo.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月7日上午11:35:02
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter15;

import java.util.Scanner;

/**
 * ClassName:StrDemo <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月7日 上午11:35:02 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: StrDemo <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月7日 上午11:35:02 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class StrDemo {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub

		StrDemo strDemo = new StrDemo();
//		strDemo.question6();		strDemo.countCharOccurs();
	}
	
	public void question5(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("--欢迎进入作业提交系统--");
		System.out.print("请输入Java文件名:");
		String fileName = scanner.next();
		System.out.print("请输入你的邮箱:");
		String mailName = scanner.next();
		
		boolean checked = true;
		if(!fileName.endsWith(".java")){
			System.out.println("文件名不合法!");
			checked = false;
		}
		
		int atIdx = mailName.indexOf('@');
		int dotIdx = mailName.indexOf('.');
		if(atIdx==-1||dotIdx==-1||atIdx>dotIdx){
			System.out.println("E-mail无效!");
			checked = false;
		}
		if(checked){
			System.out.println("作业提交成功!");
		} else {
			System.out.println("作业提交失败!");
		}
		
		scanner.close();
	}
	
	public void question66(){
		String orginalStr = "boo:and:foo";
		String[] slices = orginalStr.split(":");
		for(String s:slices){
			System.out.println(s);
		}
	}
	
	/**
	 * 
	 * question6:问题6代码. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void question6(){
		String orginalStr = "长亭外 古道边 芳草碧连天 晚风扶 柳笛声残 夕阳山外山";
		System.out.println("***原歌词格式***");
		System.out.println(orginalStr);
		System.out.println("\n***拆分后歌词格式***");
		String[] slices = orginalStr.split(" ");
		for(String s:slices){
			System.out.println(s);
		}
	}
	
	/**
	 * 
	 * countCharOccurs:判断字符出现次数. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void countCharOccurs(){
		Scanner scanner = new Scanner(System.in);
		System.out.print("请输入一个字符串:");
		String destStr = scanner.next();
		System.out.print("请输入要查找的字符:");
		String srch = scanner.next();
		
		int cnt = 0;//统计次数变量
		int fromIndex = 0;//检索开始位置
		int idx = destStr.indexOf(srch, fromIndex);
		while(idx!=-1){
			cnt++;
			fromIndex = idx + 1;
			idx = destStr.indexOf(srch, fromIndex);
		}
		
		System.out.println("\""+destStr+"\"中包含"+cnt+"个\""+srch+"\"");
		scanner.close();
	}

}

