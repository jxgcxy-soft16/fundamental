/**
 * Project Name:fundamental
 * File Name:GoodsBiz.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月9日下午3:34:02
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:GoodsBiz.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月9日下午3:34:02
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter15;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 * ClassName:GoodsBiz <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月9日 下午3:34:02 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: GoodsBiz <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月9日 下午3:34:02 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class GoodsBiz {

	Scanner scanner = new Scanner(System.in);
	NumberFormat nf = NumberFormat.getCurrencyInstance();
	
	Goods[] goodsList = new Goods[]{
			new Goods(1, "电风扇", 124.23),
			new Goods(2, "洗衣机", 4500.0),
			new Goods(3, "电视机", 8800.9),
			new Goods(4, "冰箱", 5000.88),
			new Goods(5, "空调机", 4456.0)
		};
	
	/**
	 * 
	 * start:商品操作类启动方法. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void start(){

		while(!login());
		
		this.show();
		this.change();
	}
	
	/**
	 * 
	 * login:用户登录. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @return
	 * @since JDK 1.6
	 */
	public boolean login(){
		final String CORRECT_NAME = "TOM", CORRECT_PWD = "123";
		System.out.print("请输入用户名:");
		String name = scanner.next();
		System.out.print("请输入密码:");
		String pwd = scanner.next();
		if(CORRECT_NAME.equals(name)&&CORRECT_PWD.equals(pwd)){
			System.out.println("登录成功");
			return true;
		} 
		System.out.println("登录失败");
		return false;
	}
	
	/**
	 * 
	 * show:显示商品列表信息. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void show(){
		System.out.println("*********欢迎进入商品批发城*********");
		System.out.println("\t编号\t商品\t价格");
		for(Goods gd:goodsList){
			StringBuffer sb = new StringBuffer();
			sb.append('\t').append(gd.gdno)
			  .append('\t').append(gd.gdname)
			  .append('\t').append(nf.format(gd.gdprice));
			System.out.println(sb);
		}
		System.out.println("***********************************");
	}
	
	/**
	 * 
	 * change:输入批发信息. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void change(){
		int gdno = 0;
		boolean notIn = false;
		do {
			System.out.print("请输入您批发的商品编号:");
			gdno = scanner.nextInt();
			notIn = !(gdno>0&&gdno<=goodsList.length);
			if(notIn){
				System.out.println(new StringBuffer("不存在商品编号").append(gdno));
			}
		} while(notIn);
		
		System.out.print("请输入批发数量:");
		int gdnum = scanner.nextInt();
		double payAmount = goodsList[gdno-1].gdprice*gdnum;
		System.out.println(new StringBuffer("您需要付款:").append(nf.format(payAmount)));
	}
	
	/**
	 * 
	 * format:格式化金额. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param d
	 * @return
	 * @since JDK 1.6
	 */
	private StringBuffer format(double d){
		StringBuffer sb = new StringBuffer();
		sb.append(d);
		int dotIdx = sb.indexOf(".");
		if(dotIdx==-1){
			dotIdx = sb.length();
		}
		for(int i=dotIdx-3;i>0;i-=3){
			sb.insert(i, ',');
		}
		return sb;
	}
}

