/**
 * Project Name:fundamental
 * File Name:StrBufTest.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月9日上午8:46:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:StrBufTest.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月9日上午8:46:28
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter15;

import java.util.Scanner;

/**
 * ClassName:StrBufTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月9日 上午8:46:28 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: StrBufTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月9日 上午8:46:28 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class StrBufTest {

	public static void main(String[] args){
		/*StringBuffer sb = new StringBuffer("青春无悔"); 
    	int num=110;
    	StringBuffer sb1 = sb.append("我心永恒");  
    	System.out.println(sb1);
   	 	StringBuffer sb2 = sb1.append('啊');       
    	System.out.println(sb2);
    	StringBuffer sb3 = sb2.append(num);    
    	System.out.println(sb3);*/
		new StrBufTest().question7();

	}
	
	public void question7(){
		Scanner scanner = new Scanner(System.in);
		System.out.print("请输入一串数字");
		String digitals = scanner.next();
		StringBuffer sb = new StringBuffer(digitals);
		for(int i=sb.length()-1,j=1;i>=0;i--,j++){
			if(j%3==0){
				sb.insert(i, ",");
			}
		}
		System.out.println(sb);
		scanner.close();
	}
}

