/**
 * Project Name:fundamental
 * File Name:UserRegTest.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月7日上午9:01:29
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:UserRegTest.java
 * Package Name:com.jiuxing.fundamental.chapter15
 * Date:2017年11月7日上午9:01:29
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter15;

import java.util.Scanner;

/**
 * ClassName:UserRegTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月7日 上午9:01:29 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: UserRegTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月7日 上午9:01:29 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class UserRegTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		UserRegTest urt = new UserRegTest();
		urt.checkPasswordEqual();
//		urt.checkPasswordLen();
	}
	
	/**
	 * 
	 * checkPasswordLen:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void checkPasswordLen(){
		Scanner scanner = new Scanner(System.in);
		System.out.print("请输入用户名:");
		String name = scanner.next();
		System.out.print("请输入密码:");
		String password = scanner.next();
		if(password.length()<6){
			System.out.println("密码长度不能小于6位!");
		} else {
			System.out.println("注册成功！");
		}
		scanner.close();
	}
	
	public void checkPasswordEqual(){
		String oldname = "TOM",oldpassword = "1234567";
		Scanner scanner = new Scanner(System.in);
		System.out.print("请输入用户名:");
		String name = scanner.next();
		System.out.print("请输入密码:");
		String password = scanner.next();
		if(oldname.toLowerCase().equals(name.toLowerCase())&&oldpassword.equals(password)){
			System.out.println("登录成功!");
		} else {
			System.out.println("用户名或密码不匹配，登录失败！");
		}
		scanner.close();
	}

}

