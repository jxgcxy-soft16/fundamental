/**
 * Project Name:fundamental
 * File Name:Height.java
 * Package Name:com.jiuxing.fundamental.chapter14
 * Date:2017年11月6日下午3:28:15
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Height.java
 * Package Name:com.jiuxing.fundamental.chapter14
 * Date:2017年11月6日下午3:28:15
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter14;
/**
 * ClassName:Height <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月6日 下午3:28:15 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Height <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月6日 下午3:28:15 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Height {

	public float avgHeight(Student[] stus){
		float sum = 0;
		int count = 0;
		for(int i=0;i<stus.length&&stus[i]!=null;i++){
			sum += stus[i].height;
			count++;
		}
		sum /= count;
		return sum;
	}
}

