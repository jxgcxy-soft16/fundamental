/**
 * Project Name:fundamental
 * File Name:HeightTest.java
 * Package Name:com.jiuxing.fundamental.chapter14
 * Date:2017年11月6日下午3:56:05
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:HeightTest.java
 * Package Name:com.jiuxing.fundamental.chapter14
 * Date:2017年11月6日下午3:56:05
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter14;

import java.util.Scanner;

/**
 * ClassName:HeightTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月6日 下午3:56:05 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: HeightTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月6日 下午3:56:05 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class HeightTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Height height = new Height();
		Student[] stus = new Student[5];
		Scanner scanner = new Scanner(System.in);
		
		for(int i=0;i<stus.length;i++){
			System.out.print("输入第"+(i+1)+"名学生的身高(cm):");
			Student st = new Student();
			st.height = scanner.nextFloat();
			stus[i] = st;
		}
		float avg = height.avgHeight(stus);
		System.out.println("***这"+stus.length+"名学生的平均身高为"+avg+"cm***");
		scanner.close();
	}

}

