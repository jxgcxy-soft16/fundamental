/**
 * Project Name:fundamental
 * File Name:FistRule.java
 * Package Name:com.jiuxing.fundamental.chapter12
 * Date:2017年10月25日下午3:29:07
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:FistRule.java
 * Package Name:com.jiuxing.fundamental.chapter12
 * Date:2017年10月25日下午3:29:07
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.jiuxing.fundamental.chapter12;
/**
 * ClassName:FistRule <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月25日 下午3:29:07 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: FistRule <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月25日 下午3:29:07 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class FistRule {
	
	
	/**
     * 
     * compareFist:猜拳计算结果 . <br>
     * TODO(这里描述这个方法适用条件 – 可选).<br>
     * TODO(这里描述这个方法的执行流程 – 可选).<br>
     * TODO(这里描述这个方法的使用方法 – 可选).<br>
     * TODO(这里描述这个方法的注意事项 – 可选).<br>
     * 
     * 出拳规则:1.剪刀 2.石头 3.布
     * 
     * @author wengouliang
     * @param humanFist
     * @param computerFist
     * @return 等于0 - 平局, 大于0 - 赢, 小于0 - 输 
     * @since JDK 1.6
     */
    public int compareFist(int humanFist, int computerFist){
    	int diff = humanFist - computerFist;
    	int diffAbs = Math.abs(diff);
    	if(diffAbs<=1){
    		return diff;
    	} else {
    		return diff*(-1);
    	}
    }

}

