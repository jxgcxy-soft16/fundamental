package com.jiuxing.fundamental.chapter12;

import java.util.Scanner;

/**
 * 
 * ClassName: Game <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月25日 上午10:58:19 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Game {
    private int count = 0;
    private Human human = new Human();
    private Computer computer = new Computer();
    FistRule rule = new FistRule();
    Scanner scanner = new Scanner(System.in);

    /**
     * 
     * initial:初始化相关角色信息. <br>
     * TODO(这里描述这个方法适用条件 – 可选).<br>
     * TODO(这里描述这个方法的执行流程 – 可选).<br>
     * TODO(这里描述这个方法的使用方法 – 可选).<br>
     * TODO(这里描述这个方法的注意事项 – 可选).<br>
     *
     * @author wengouliang
     * @since JDK 1.6
     */
    public void initial(){
    	System.out.println("--------------欢迎进入游戏世界------------\n");
    	System.out.println("             ****************            ");
    	System.out.println("             **  猜拳,开始   **            ");
    	System.out.println("             ****************            ");
    	System.out.println("\n\n出拳规则:1.剪刀 2.石头 3.布");
    	
    	computer.chooseRole(scanner);
    	human.inputAccount();
    }

    public void startGame(){
    	initial();//初始化信息
    	play();//对战
    	printFinalResult();//打印结果
    }
    
    /**
     * 
     * play:游戏对战. <br>
     * TODO(这里描述这个方法适用条件 – 可选).<br>
     * TODO(这里描述这个方法的执行流程 – 可选).<br>
     * TODO(这里描述这个方法的使用方法 – 可选).<br>
     * TODO(这里描述这个方法的注意事项 – 可选).<br>
     *
     * @author wengouliang
     * @since JDK 1.6
     */
    public void play(){
    	System.out.print("\n要开始吗? (y/n)");
    	String start = scanner.next();
    	while("y".equals(start)){
    		this.count++;
    		int humanFist = this.human.showFist();
        	int computerFist = this.computer.showFist();
        	showCurrentResult(humanFist, computerFist);
        	
        	System.out.println("\n是否开始下一轮(y/n):");
        	start = scanner.next();
    	} 
    }
    
    /**
     * 
     * showCurrentResult:显示当前对战结果. <br>
     * TODO(这里描述这个方法适用条件 – 可选).<br>
     * TODO(这里描述这个方法的执行流程 – 可选).<br>
     * TODO(这里描述这个方法的使用方法 – 可选).<br>
     * TODO(这里描述这个方法的注意事项 – 可选).<br>
     *
     * @author wengouliang
     * @param humanFist
     * @param computerFist
     * @since JDK 1.6
     */
    public void showCurrentResult(int humanFist, int computerFist){
		int res = rule.compareFist(humanFist, computerFist);
		if(res>0){
			human.score++;
			System.out.println("结果说:恭喜,你赢了，真棒!");
		} else if(res==0){
			System.out.println("结果说:和局,真衰!嘿嘿,等着瞧吧!");
		} else {
			computer.score++;
			System.out.println("结果说:^_^,你输了，真笨!");
		}
	}
    
    /**
     * 
     * printFinalResult:打印最终结果. <br>
     * TODO(这里描述这个方法适用条件 – 可选).<br>
     * TODO(这里描述这个方法的执行流程 – 可选).<br>
     * TODO(这里描述这个方法的使用方法 – 可选).<br>
     * TODO(这里描述这个方法的注意事项 – 可选).<br>
     *
     * @author wengouliang
     * @since JDK 1.6
     */
    public void printFinalResult(){
    	System.out.println("---------------------------------------\n");
    	System.out.println(computer.name+"\tVS\t"+human.name);
    	System.out.println("对战次数:\t"+count);
    	System.out.println("\n姓名\t得分");
    	System.out.println(human.name+"\t"+human.score);
    	System.out.println(computer.name+"\t"+computer.score);
    	
    	String resultStr = "恭喜你,赢得整个对战!";
    	if(human.score==computer.score){
    		resultStr = "打成平手,下次再与你一分高下!";
    	} else if (human.score<computer.score){
    		resultStr = "呵呵,笨笨,下次加油啊!";
    	}
    	System.out.println("\n结果:"+resultStr);
    }
    
    public static void main(String[] args){
    	Game game = new Game();
    	game.startGame();
    }
    
    

}
