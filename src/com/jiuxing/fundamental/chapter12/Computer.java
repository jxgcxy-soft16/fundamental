package com.jiuxing.fundamental.chapter12;

import java.util.Random;
import java.util.Scanner;

public class Computer {
     String name = "电脑";
     int score = 0;

    public int showFist(){
    	Random random = new Random();
    	int fist = random.nextInt(3)+1;
    	String fistName = "";
    	switch(fist){
    	case 1:
    		fistName = "剪刀";
    		break;
    	case 2:
    		fistName = "石头";
    		break;
    	case 3:
    		fistName = "布";
    		break;
    	}
    	System.out.println(name+"出拳:"+fistName);
        return fist;
    }
    
    public void chooseRole(Scanner scanner){
    	System.out.print("请选择对方角色(1:刘备 2.孙权 3.曹操):");
    	
    	String roleName = "";
    	int roleId = scanner.nextInt();
    	switch(roleId){
    	case 1:
    		roleName = "刘备";
    		break;
    	case 2:
    		roleName = "孙权";
    		break;
    	case 3:
    		roleName = "曹操";
    		break;
    	}
    	this.name = roleName;
    	System.out.println("你选择了"+roleName+"对战");
    	
    }

}
