package com.jiuxing.fundamental.chapter12;

import java.util.Scanner;

public class Human {
     String name;
     int score = 0;
     Scanner scanner = new Scanner(System.in);
     
    public int showFist(){
    	
    	System.out.print("\n请出拳:1.剪刀 2.石头 3.布 (输入相应数字):");
    	
    	boolean loop = false;
    	String fistName = "";
    	int fist = scanner.nextInt();
    	do {
    		switch(fist){
        	case 1:
        		fistName = "剪刀";
        		break;
        	case 2:
        		fistName = "石头";
        		break;
        	case 3:
        		fistName = "布";
        		break;
        	default:
        		System.out.println("出拳错误,请重新出拳！");
        		loop = true;
        	}
    	} while(loop);
    	
    	System.out.println("你出拳:"+fistName);
        return fist;
    }
    
    /**
     * 
     * inputAccount:输入账号信息. <br>
     * TODO(这里描述这个方法适用条件 – 可选).<br>
     * TODO(这里描述这个方法的执行流程 – 可选).<br>
     * TODO(这里描述这个方法的使用方法 – 可选).<br>
     * TODO(这里描述这个方法的注意事项 – 可选).<br>
     *
     * @author wengouliang
     * @since JDK 1.6
     */
    public void inputAccount(){
    	System.out.print("请输入你的姓名:");
    	this.name = scanner.next();
    }

}
