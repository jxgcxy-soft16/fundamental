/**
 * Project Name:fundamental
 * File Name:MemberBiz.java
 * Package Name:com.wxws.sms
 * Date:2017年11月6日上午11:02:41
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MemberBiz.java
 * Package Name:com.wxws.sms
 * Date:2017年11月6日上午11:02:41
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.wxws.sms;

import java.util.Scanner;

/**
 * ClassName:MemberBiz <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月6日 上午11:02:41 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MemberBiz <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月6日 上午11:02:41 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class MemberBiz {

	Member[] members = new Member[5];
	
	Scanner scanner = new Scanner(System.in);
	
	/**
	 * 
	 * inputMember:从控制台录入会员. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @return
	 * @since JDK 1.6
	 */
	public boolean inputMember(){
		boolean added = false;//添加是否成功
		Member member = new Member();
		System.out.print("输入会员编号:");
		member.num = scanner.nextInt();
		System.out.print("输入会员积分:");
		member.score = scanner.nextInt();
		added = addMember(member);
		return added;
	}
	
	/**
	 * 
	 * inputMemberBatch:批量添加会员. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void inputMemberBatch(){
		boolean added = true;
		for(int i=0;i<this.members.length&&added;i++){
			added = inputMember();
		}
	}
	
	/**
	 * 
	 * addMember:添加会员到数组. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param member
	 * @return
	 * @since JDK 1.6
	 */
	public boolean addMember(Member member){
		int i=0;
		for(;i<members.length&&members[i]!=null;i++);
		
		if(i==members.length){
			System.out.println("数组已填满,不能再加值");
			return false;
		} else {
			members[i] = member;
			return true;
		}
	}
	
	/**
	 * 
	 * showMembers:显示所有会员. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void showMembers(){
		System.out.println("***会员列表***");
		System.out.println("编号\t积分");
		for(int i=0;i<members.length&&members[i]!=null;i++){
			System.out.println(members[i].num+"\t"+members[i].score);
		}
	}
	
	/**
	 * 
	 * search:查找会员. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void search(){
		System.out.print("请输入要查找的会员编号:");
		int num = scanner.nextInt();
		Member memberToFind = null;
		for(int i=0;i<this.members.length&&this.members[i]!=null;i++){
			if(num==this.members[i].num){
				memberToFind = this.members[i];
				break;
			}
		}
		if(null==memberToFind){
			System.out.println("没有此编号的会员...");
		} else {
			System.out.println("该会员积分为:"+memberToFind.score);
		}
	}
}

