/**
 * Project Name:fundamental
 * File Name:Member.java
 * Package Name:com.wxws.sms
 * Date:2017年11月6日上午11:00:53
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Member.java
 * Package Name:com.wxws.sms
 * Date:2017年11月6日上午11:00:53
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.wxws.sms;
/**
 * ClassName:Member <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月6日 上午11:00:53 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Member <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月6日 上午11:00:53 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Member {

	/**
	 * 会员编号
	 */
	int  num;
	
	/**
	 * 会员积分
	 */
	int  score;
}

