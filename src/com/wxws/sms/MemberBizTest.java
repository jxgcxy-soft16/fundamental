/**
 * Project Name:fundamental
 * File Name:MemberBizTest.java
 * Package Name:com.wxws.sms
 * Date:2017年11月6日上午11:24:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MemberBizTest.java
 * Package Name:com.wxws.sms
 * Date:2017年11月6日上午11:24:43
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package com.wxws.sms;
/**
 * ClassName:MemberBizTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年11月6日 上午11:24:43 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MemberBizTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年11月6日 上午11:24:43 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class MemberBizTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub

		MemberBiz memberBiz = new MemberBiz();
//		memberBiz.inputMember();
		memberBiz.inputMemberBatch();
		memberBiz.showMembers();
		memberBiz.search();
	}

}

