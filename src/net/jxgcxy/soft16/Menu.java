/**
 * Project Name:fundamental
 * File Name:Menu.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午8:06:14
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Menu.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午8:06:14
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:Menu <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月12日 下午8:06:14 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Menu <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月12日 下午8:06:14 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Menu {

	String title;
	MenuItem[] items;
	String inputTip;
	int itemIdActive;
	
	Menu prevMenu;
	
	public void show(){
		System.out.println(title);
		System.out.println("***********************************");
		for(int i=0;i<items.length;i++){
			items[i].show();
		}
		System.out.println("***********************************");
		if(null!=inputTip){
			System.out.println(inputTip);
		}
	}
	
	public void select(){
		if(0==itemIdActive){
			this.prevMenu.show();
			return;
		}
		for(int i=0;i<items.length;i++){
			if(itemIdActive==items[i].itemId){
				items[i].doAction();
				break;
			}
		}
	}
	
}

