/**
 * Project Name:fundamental
 * File Name:Customer.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日下午4:46:55
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Customer.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日下午4:46:54
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:Customer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月11日 下午4:46:55 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Customer <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月11日 下午4:46:54 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Customer {
	/**
	 * 积分
	 */
	int score;
	
	/**
	 * 卡类型
	 */
	String cardType;
	
	/**
	 * 
	 * show:显示顾客信息. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void show(){
		System.out.println("customer's score="+score+",cardType="+cardType);
	}
	
	/**
	 * 
	 * feedback:积分回馈. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void feedback(){
		if((this.cardType.equals("金卡")&&this.score>1000)
				||(this.cardType.equals("普卡")&&this.score>5000)){
			System.out.println("回馈积分500分!");
		} else {
			System.out.println("没有回馈积分!");
		}
	}
}

