/**
 * Project Name:fundamental
 * File Name:AdministratorDemo.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日上午10:42:56
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:AdministratorDemo.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日上午10:42:56
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:AdministratorDemo <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月11日 上午10:42:56 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: AdministratorDemo <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月11日 上午10:42:56 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class AdministratorDemo {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		/**
		 * 创建管理员对象
		 */
		Administrator administator = new Administrator();
		
		/**
		 * 给对象属性赋值
		 */
		administator.name = "Tom";
		administator.password = "123";
		
		/**
		 * 调用对象方法打印出信息
		 */
		administator.showInfo();
		
		/**
		 * 修改管理员密码
		 */
		administator.chgPwd();
		
		administator.showInfo();
		
		/**
		 * 创建管理员对象
		 *//*
		Administrator administator2 = new Administrator();
		
		*//**
		 * 给对象属性赋值
		 *//*
		administator2.name = "Jerry";
		administator2.password = "456";
		
		*//**
		 * 调用对象方法打印出信息
		 *//*
		administator2.showInfo();*/

	}

}

