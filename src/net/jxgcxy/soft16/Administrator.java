/**
 * Project Name:fundamental
 * File Name:Administrator.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日上午10:39:00
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Administrator.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日上午10:39:00
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;

import java.util.Scanner;

/**
 * ClassName:Administrator <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月11日 上午10:39:00 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Administrator <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月11日 上午10:39:00 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Administrator {

	/**
	 * 姓名
	 */
	String name;
	
	/**
	 * 密码
	 */
	String password;
	
	/**
	 * 
	 * showInfo:显示管理员信息. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void showInfo(){
		System.out.println("Administator's name="+name+",password="+password);
	}
	
	/**
	 * 
	 * chgPwd:修改密码. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void chgPwd(){
		Scanner scanner = new Scanner(System.in);
		System.out.print("请输入用户名:");
		String iname = scanner.next();
		System.out.print("请输入用户密码:");
		String pwd = scanner.next();
		if(iname.equals(name)&&pwd.equals(password)){
			String newpwd = "";
			String newpwd2 = "";
			boolean isSame = false;
			do {
				System.out.print("请输入新密码:");
				newpwd = scanner.next();
				System.out.print("请再次输入新密码:");
				newpwd2 = scanner.next();
				isSame = newpwd.equals(newpwd2);
				if(isSame){
					this.password = newpwd;
					System.out.println("修改密码成功,您的新密码为:"+this.password);
				} else {
					System.out.println("您两次输入的密码不一致！");
				}
			} while(!isSame);
		} else {
			System.out.println("用户名或密码不正确!");
		}
		scanner.close();
	}
}

