/**
 * Project Name:fundamental
 * File Name:MenuItem.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午8:02:31
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MenuItem.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午8:02:31
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:MenuItem <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月12日 下午8:02:31 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MenuItem <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月12日 下午8:02:31 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class MenuItem {
	int itemId;
	String itemName;
	MenuItemAction menuItemAction;
	
	public void show(){
		System.out.println(itemId+". "+itemName);
	}
	
	public void doAction(){
		if(null!=menuItemAction){
			menuItemAction.doAction();
		}
	}
}

