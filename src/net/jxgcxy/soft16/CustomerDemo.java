/**
 * Project Name:fundamental
 * File Name:CustomerDemo.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日下午4:52:06
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:CustomerDemo.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月11日下午4:52:06
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:CustomerDemo <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月11日 下午4:52:06 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: CustomerDemo <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月11日 下午4:52:06 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class CustomerDemo {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		/**
		 * 创建顾客对象
		 */
		Customer customer = new Customer();
		
		/**
		 * 对对象发生赋值
		 */
		customer.score = 1880;
		customer.cardType = "金卡";
		
		/**
		 * 显示对象信息
		 */
		customer.show();
		
		/**
		 * 回馈积分
		 */
		customer.feedback();

	}

}

