/**
 * Project Name:fundamental
 * File Name:Cell.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午2:55:22
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:Cell.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午2:55:22
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:Cell <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月12日 下午2:55:22 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: Cell <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月12日 下午2:55:22 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class Cell {
	
	/**
	 * 品牌
	 */
	String brand;
	
	/**
	 * 
	 * fillElec:续电. <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @since JDK 1.6
	 */
	public void fillElec(){
		System.out.println(brand+" 续电...");
	}
}

