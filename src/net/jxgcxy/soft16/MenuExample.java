/**
 * Project Name:fundamental
 * File Name:MenuExample.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午10:30:35
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MenuExample.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午10:30:35
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;

import java.util.Scanner;

/**
 * ClassName:MenuExample <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月12日 下午10:30:35 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MenuExample <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月12日 下午10:30:35 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class MenuExample {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Menu indexMenu = new Menu();
		Menu mainMenu = new Menu();
		Menu feedMenu = new Menu();
		
		mainMenu.prevMenu = indexMenu;
		feedMenu.prevMenu = mainMenu;
		
		
		feedMenu.title = "我行我素购物管理系统> 真情回馈";
		MenuItem menuItem5 = new MenuItem();
		menuItem5.itemId = 1;
		menuItem5.itemName = "幸运大放送";
		
		MenuItem menuItem6 = new MenuItem();
		menuItem6.itemId = 2;
		menuItem6.itemName = "幸运抽奖";
		feedMenu.items = new MenuItem[]{
				menuItem5, menuItem6
		};
		
		mainMenu.title = "我行我素购物管理系统主菜单";
		mainMenu.inputTip = "请选择，输入数字或按0返回上一级菜单:";

		MenuItem menuItem3 = new MenuItem();
		menuItem3.itemId = 1;
		menuItem3.itemName = "客户信息管理";
		menuItem3.menuItemAction = new MenuItemAction(){
			@Override
			public void doAction() {
				
			}
		};
		
		MenuItem menuItem4 = new MenuItem();
		menuItem4.itemId = 2;
		menuItem4.itemName = "真情回馈";
		menuItem4.menuItemAction = new MenuItemAction(){
			@Override
			public void doAction() {
				
				feedMenu.show();
				feedMenu.itemIdActive = scanner.nextInt();
//				feedMenu.select();
				
			}
		};
		
		mainMenu.items = new MenuItem[]{
				menuItem3, menuItem4
		};
		
		
		indexMenu.title = "欢迎使用我行我素购物系统";
		indexMenu.inputTip = "请选择，输入数字:";
		MenuItem menuItem = new MenuItem();
		menuItem.itemId = 1;
		menuItem.itemName = "登录系统";
		menuItem.menuItemAction = new MenuItemAction(){
			@Override
			public void doAction() {
				mainMenu.show();
				mainMenu.itemIdActive = scanner.nextInt();
				mainMenu.select();
			}
		};
		
		MenuItem menuItem2 = new MenuItem();
		menuItem2.itemId = 2;
		menuItem2.itemName = "退出";
		menuItem2.menuItemAction = new MenuItemAction(){
			@Override
			public void doAction() {
				
				// TODO Auto-generated method stub
				
			}
		};
		
		indexMenu.items = new MenuItem[]{
				menuItem, menuItem2
		};
		
		indexMenu.show();
		indexMenu.itemIdActive = scanner.nextInt();
		indexMenu.select();
		
		scanner.close();
	}

}

