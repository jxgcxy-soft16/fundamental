/**
 * Project Name:fundamental
 * File Name:MenuItemAction.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午10:01:54
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:MenuItemAction.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午10:01:54
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:MenuItemAction <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月12日 下午10:01:54 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: MenuItemAction <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月12日 下午10:01:54 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public interface MenuItemAction {
	void doAction();
}

