/**
 * Project Name:fundamental
 * File Name:ScoreCalcTest.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午4:05:47
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
*/
/**
 * Project Name:fundamental
 * File Name:ScoreCalcTest.java
 * Package Name:net.jxgcxy.soft16
 * Date:2017年10月12日下午4:05:47
 * Copyright (c) 2017, wengouliang@qq.com All Rights Reserved.
 *
 */

package net.jxgcxy.soft16;
/**
 * ClassName:ScoreCalcTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason:	 TODO ADD REASON. <br>
 * Date:     2017年10月12日 下午4:05:47 <br>
 * @author   wengouliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ScoreCalcTest <br>
 * Function: TODO ADD FUNCTION. <br>
 * Reason: TODO ADD REASON(可选). <br>
 * date: 2017年10月12日 下午4:05:47 <br>
 *
 * @author wengouliang
 * @version 
 * @since JDK 1.6
 */
public class ScoreCalcTest {

	/**
	 * main:(这里用一句话描述这个方法的作用). <br>
	 * TODO(这里描述这个方法适用条件 – 可选).<br>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br>
	 *
	 * @author wengouliang
	 * @param args
	 * @since JDK 1.6
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub

		ScoreCalc scoreCalc = new ScoreCalc();
		
		/**
		 * 录入成绩
		 */
		scoreCalc.input();
		
		/**
		 * 求总成绩
		 */
		int sm = scoreCalc.sum();
		System.out.println("总成绩是: "+sm);
		
		int av = scoreCalc.avg();
		System.out.println("平均成绩是: "+av);
	}

}

